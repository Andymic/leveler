﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Common.Model.TradeModel
{
    public class TradeDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public bool Active { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Category { get; set; }
    }
}
