﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Common.Helpers;

namespace Common.Model.HouseModel
{
    public class HouseContractDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        [DataMember]
        public DateTime DateModified { get; set; }

        [DataMember]
        public DateTime DueDate { get; set; }

        [DataMember]
        public int HouseId;

        [DataMember]
        public LandlordDTO Landlord;

        [DataMember]
        public int LandlordId { get; set; }

        [DataMember]
        public IEnumerable<UserDTO> Tenants { get; set; }

        [DataMember]
        public ContractStatus Status { get; set; }

        [DataMember]
        public string Contract { get; set; }
    }
}
