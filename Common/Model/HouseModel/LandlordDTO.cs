﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model.HouseModel
{
    public class LandlordDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        [DataMember]
        public DateTime DateModified { get; set; }

        [DataMember]
        public int UserId { get; set; }

        public IEnumerable<HouseDTO> Houses { get; set; } 
    }
}
