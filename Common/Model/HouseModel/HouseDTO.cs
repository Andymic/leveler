﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Common.Helpers;

namespace Common.Model.HouseModel
{
    public class HouseDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        [DataMember]
        public DateTime DateModified { get; set; }

        //[DataMember]
        //public Location Location { get; set; }

        [DataMember]
        public string Details { get; set; }

        [DataMember]
        public List<Utilities> Utilities { get; set; }

        [DataMember]
        public int LandlordId { get; set; }

        public IEnumerable<UserDTO> Tenants { get; set; }
    }
}
