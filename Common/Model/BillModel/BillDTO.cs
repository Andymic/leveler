﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Common.Model.BillModel
{
    public class BillDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int HouseId;

        [DataMember]
        public int[] UserId;

        [DataMember]
        public string Amount { get; set; }

        [DataMember]
        public string Nickname { get; set; }

        [DataMember]
        public string Reminder { get; set; }

        [DataMember]
        public DateTime DueDate { get; set; }

        [DataMember]
        public string Status { get; set; }
    }
}
