﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Common.Model.DealModel
{
    public class DealDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int HouseId;

        [DataMember]
        public string Company { get; set; }

        [DataMember]
        public string Category { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public DateTime Expire { get; set; }

        [DataMember]
        public string Status { get; set; }
    }
}
