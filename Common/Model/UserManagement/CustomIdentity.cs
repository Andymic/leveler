﻿using Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model.UserManagement
{
    public class CustomIdentity:ClaimsIdentity
    {
        private readonly ClaimsIdentity _claimsIdentity;

        public CustomIdentity(ClaimsIdentity claimsIdent)
        {
            this._claimsIdentity = claimsIdent;
            this.AddClaims(claimsIdent.Claims);
        }

        public override string AuthenticationType
        {
            get { return _claimsIdentity.AuthenticationType; }
        }

        public override bool IsAuthenticated
        {
            get { return _claimsIdentity.IsAuthenticated; }
        }

        public override string Name
        {
            get { return _claimsIdentity.Name; }
        }

        public string Email
        {
            get 
            { 
                var claim = _claimsIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email);
                return claim != null ? claim.Value : String.Empty;
            }
        }

        public int Id
        {
            get
            {
                var claim = _claimsIdentity.Claims.FirstOrDefault(c => c.Type == "Id");
                return claim != null ? int.Parse(claim.Value) : -1;
            }
        }

        public int LandlordId
        {
            get
            {
                var claim = _claimsIdentity.Claims.FirstOrDefault(c => c.Type == "LandlordId");
                return claim != null ? int.Parse(claim.Value) : -1;
            }
        }

        public UserType UserType
        {
            get
            {
                var claim = _claimsIdentity.Claims.FirstOrDefault(c => c.Type == "UserType");
                return claim != null ? (UserType)Enum.Parse(typeof(UserType), claim.Value) : UserType.None;
            }
        }
    }
}
