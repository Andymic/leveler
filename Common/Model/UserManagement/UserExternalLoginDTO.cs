﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model.UserManagement
{
    public class UserExternalLoginDTO
    {
        [DataMember]
        public string LoginProvider { get; set; }

        [DataMember]
        public string ProviderKey { get; set; }

        [DataMember]
        public int UserId { get; set; }

        public UserExternalLoginDTO() { }

        public UserExternalLoginDTO(string loginProvider, string providerKey, int? userId)
        {
            this.LoginProvider = loginProvider;
            this.ProviderKey = providerKey;

            if(userId.HasValue)
                this.UserId = userId.Value;
        }
    }
}
