﻿using Common.Helpers;
using Common.Model.UserManagement;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model
{
    public class UserDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        [DataMember]
        public DateTime DateModified { get; set; }

        [DataMember]
        public DateTime LastLogin { get; set; }

        [DataMember]
        public UserType UserType { get; set; }

        [DataMember]
        public int? HouseId { get; set; }
        
        [DataMember]
        public int? LandlordId { get; set; }
    }

    public class IdentityUserDTO : IdentityUser, IUser<int>
    {
        [DataMember]
        new public int Id { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        [DataMember]
        public DateTime DateModified { get; set; }

        [DataMember]
        public DateTime LastLogin { get; set; }

        [DataMember]
        public int? HouseId { get; set; }

        [DataMember]
        public int? LandlordId { get; set; }

        [DataMember]
        public UserType UserType { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<IdentityUserDTO, int> manager, string authenticationType = DefaultAuthenticationTypes.ExternalBearer)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity; //new CustomIdentity(userIdentity);
        }
    }
}
