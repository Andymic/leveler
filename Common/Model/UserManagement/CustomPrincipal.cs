﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model.UserManagement
{
    public class CustomPrincipal:ClaimsPrincipal
    {
        private readonly CustomIdentity _identity;

        public CustomPrincipal(IIdentity identity)
        {
            this._identity = new CustomIdentity((ClaimsIdentity)identity);
            this.AddIdentities(new List<CustomIdentity>() { this.Identity });
        }

        new public CustomIdentity Identity
        {
            get { return _identity; }
        }

        public override bool IsInRole(string role)
        {
            return base.IsInRole(role);
        }
    }
}
