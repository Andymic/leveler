﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Data
{
    public class BaseRepository<T, TContext> : IDisposable
        where T : class
        where TContext : DbContext
    {
        protected TContext Context;

        private IDbSet<T> Entities
        {
            get { return this.Context.Set<T>(); }
        }

        public BaseRepository(TContext context)
        {
            this.Context = context;
        }

        public IQueryable<T> Get()
        {
            return Entities.AsQueryable();
        }

        public T Get(object id)
        {
            return Entities.Find(id);
        }

        public void Add(T entity)
        {
            Entities.Add(entity);

            this.Context.SaveChanges();
        }

        public void Update(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException("Entity");
            this.Context.SaveChanges();
        }

        public void Delete(T entity)
        {
            Entities.Remove(entity);
            this.Context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.Context != null)
                {
                    this.Context.Dispose();
                    this.Context = null;
                }
            }
        }

        public bool Exists(T entity)
        {
            if (Entities.Any(x => x == entity))
                return true;
            return false;
        }

        public IEnumerable<T> Where(Func<T, bool> query)
        {
            return Entities.Where(query);
        }

        public T FirstOrDefault(Func<T, bool> query)
        {
            return Entities.FirstOrDefault(query);
        }
    }
}
