﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Data
{
    public interface IBaseRepository<T> : IDisposable where T : class
    {
        IQueryable<T> Get();
        T Get(object id);
        bool Exists(T entity);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        IEnumerable<T> Where(Func<T, bool> query);
        T FirstOrDefault(Func<T, bool> query);
    }
}
