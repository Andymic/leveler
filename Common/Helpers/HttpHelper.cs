﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Common.Helpers
{
    public static class HttpHelper
    {
        /// <summary>
        /// Utility: Gets a JWT-aware HttpClient.
        /// </summary>
        /// <param name="hostUrl">The service host url.</param>
        /// <param name="acceptMediaType"></param>
        /// <returns>HttpClient instance with JWT support.</returns>
        public static HttpClient GetJwtHttpClient(string hostUrl, string acceptMediaType = AcceptMediaTypes.ApplicationXml)
        {
            Contract.Requires(!string.IsNullOrEmpty(hostUrl));
            Contract.Ensures(Contract.Result<HttpClient>() != null);

            return GetJwtHttpClient(new Uri(hostUrl), acceptMediaType);
        }

        /// <summary>
        /// Utility: Gets a JWT-aware HttpClient.
        /// </summary>
        /// <param name="jwtCookies">JWT cookie collection to use.</param>
        /// <param name="hostUrl">The service host url.</param>
        /// <param name="acceptMediaType"></param>
        /// <returns>HttpClient instance with JWT support.</returns>
        public static HttpClient GetJwtHttpClient(CookieCollection jwtCookies, string hostUrl, string acceptMediaType = AcceptMediaTypes.ApplicationXml)
        {
            Contract.Requires(!string.IsNullOrEmpty(hostUrl));
            Contract.Ensures(Contract.Result<HttpClient>() != null);

            return GetJwtHttpClient(jwtCookies, new Uri(hostUrl), acceptMediaType);
        }

        /// <summary>
        /// Utility: Gets a JWT-aware HttpClient.
        /// </summary>
        /// <param name="host">The service host uri.</param>
        /// <param name="acceptMediaType">Accept header value.</param>
        /// <returns>HttpClient instance with JWT support.</returns>
        public static HttpClient GetJwtHttpClient(Uri host, string acceptMediaType = AcceptMediaTypes.ApplicationXml)
        {
            Contract.Requires(host != null);
            Contract.Requires(!string.IsNullOrEmpty(acceptMediaType));
            Contract.Ensures(Contract.Result<HttpClient>() != null);

            return GetJwtHttpClient(GetJwtSecurityCookies(host), host, acceptMediaType);
        }

        /// <summary>
        /// Utility: Gets a JWT-aware HttpClient.
        /// </summary>
        /// <param name="jwtCookies">JWT cookie collection to use.</param>
        /// <param name="host">The service host uri.</param>
        /// <param name="acceptMediaType">Accept header value.</param>
        /// <returns>HttpClient instance with JWT support.</returns>
        public static HttpClient GetJwtHttpClient(CookieCollection jwtCookies, Uri host, string acceptMediaType = AcceptMediaTypes.ApplicationXml)
        {
            Contract.Requires(host != null);
            Contract.Requires(!string.IsNullOrEmpty(acceptMediaType));
            Contract.Ensures(Contract.Result<HttpClient>() != null);

            var handler = new HttpClientHandler { UseCookies = true, CookieContainer = new CookieContainer(), Proxy = WebRequest.DefaultWebProxy, AutomaticDecompression = DecompressionMethods.GZip };
            var client = new HttpClient(handler);
            client.Timeout = TimeSpan.FromMinutes(5);

            if (jwtCookies != null && jwtCookies.Count != 0)
            {
                handler.CookieContainer.Add(host, jwtCookies);
            }

            client.BaseAddress = host;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(acceptMediaType));
            client.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue() { NoCache = true, NoStore = true };

            return client;
        }

        /// <summary>
        /// Utility: Converts <see cref="System.Web.HttpCookie"/> from <paramref name="httpCookies"/> to <see cref="System.Net.Cookie"/>.
        /// </summary>
        /// <param name="httpCookies">HttpCookieCollection cookie source.</param>
        /// <param name="host">Uri destination.</param>
        /// <returns>CookieCollection of converted cookies.</returns>
        public static CookieCollection ConvertHttpCookies(HttpCookieCollection httpCookies, Uri host)
        {
            Contract.Requires(httpCookies != null);
            Contract.Requires(host != null);
            Contract.Ensures(Contract.Result<CookieCollection>() != null);

            var cookies = new CookieCollection();
            foreach (string name in httpCookies)
            {
                HttpCookie cookie = httpCookies[name];

                Cookie c = new Cookie();
                c.Expires = cookie.Expires;
                c.HttpOnly = cookie.HttpOnly;
                c.Name = cookie.Name;
                c.Path = cookie.Path;
                c.Secure = cookie.Secure;
                c.Value = cookie.Value;
                c.Domain = host.Host;

                cookies.Add(c);
            }

            return cookies;
        }

        /// <summary>
        /// Generates an error message from an HTTP response.
        /// </summary>
        /// <param name="serviceResponse"></param>
        /// <returns></returns>
        private static string ErrorMessage(HttpResponseMessage serviceResponse)
        {
            string errorFormat = "Error calling service at uri {0}, status = {1}, reason = {2}";
            return string.Format(errorFormat, serviceResponse.RequestMessage.RequestUri, serviceResponse.StatusCode.ToString(), serviceResponse.ReasonPhrase);
        }

        /// <summary>
        /// Utility: Pulls JWT authorization token cookies from the current principal.
        /// </summary>
        /// <param name="host">Uri destination.</param>
        /// <returns>CookieCollection of converted cookies.</returns>
        private static CookieCollection GetJwtSecurityCookies(Uri host)
        {
            Contract.Requires(host != null);
            Contract.Ensures(Contract.Result<CookieCollection>() != null);

            var principal = Thread.CurrentPrincipal as CookiePrincipal;
            if (principal == null)
            {
                return new CookieCollection();
            }

            return ConvertHttpCookies(principal.TokenCookies, host);
        }

    }
}
