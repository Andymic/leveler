﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IdentityModel.Selectors;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public static class AuthenticationConfiguration
    {
        /// <summary>
        /// Issuer token resolver.
        /// </summary>
        private static SecurityTokenResolver issuerTokenResolver;

        /// <summary>
        /// Cookie handler.
        /// </summary>
        private static CookieHandler cookieHandler;

        /// <summary>
        /// Security token handler.
        /// </summary>
        private static SecurityTokenHandler tokenHandler;

        /// <summary>
        /// Gets the IssuerTokenResolver from the identity configuration.
        /// </summary>
        public static SecurityTokenResolver IssuerTokenResolver
        {
            get
            {
                Contract.Ensures(Contract.Result<SecurityTokenResolver>() != null);

                if (issuerTokenResolver == null)
                {
                    issuerTokenResolver = FederatedAuthentication.FederationConfiguration
                                                                    .With(x => x.IdentityConfiguration)
                                                                    .With(x => x.IssuerTokenResolver);
                    if (issuerTokenResolver == null)
                    {
                        throw new AuthenticationException("The Issuer Token Resolver has not been configured.");
                    }
                }

                return issuerTokenResolver;
            }
        }

        /// <summary>
        /// Gets the cookie handler from the authentication configuration.
        /// </summary>
        public static CookieHandler CookieHandler
        {
            get
            {
                Contract.Ensures(Contract.Result<CookieHandler>() != null);

                if (cookieHandler == null)
                {
                    cookieHandler = FederatedAuthentication.FederationConfiguration.With(x => x.CookieHandler);
                    if (cookieHandler == null)
                    {
                        throw new AuthenticationException("The Cookie Handler has not been configured.");
                    }
                }

                return cookieHandler;
            }
        }

        /// <summary>
        /// Gets the JWT security token handler from the identity configuration.
        /// </summary>
        public static SecurityTokenHandler TokenHandler
        {
            get
            {
                Contract.Ensures(Contract.Result<SecurityTokenHandler>() != null);

                if (tokenHandler == null)
                {
                    tokenHandler = FederatedAuthentication.FederationConfiguration
                                                            .With(x => x.IdentityConfiguration)
                                                            .With(x => x.SecurityTokenHandlers)
                                                            .With(x => x[typeof(JwtSecurityToken)])
                                                            .If(x => x.CanWriteToken);
                    if (tokenHandler == null)
                    {
                        throw new AuthenticationException("There is no Security Token Handler available to write JSON Web Tokens.");
                    }
                }

                return tokenHandler;
            }
        }

        /// <summary>
        /// Gets the security key used to sign tokens.
        /// </summary>
        /// <param name="issuerName"></param>
        /// <returns></returns>
        public static SecurityKey GetSecurityKey(string issuerName)
        {
            Contract.Ensures(Contract.Result<SecurityKey>() != null);

            SecurityKey securityKey = IssuerTokenResolver.ResolveSecurityKey(new NamedKeySecurityKeyIdentifierClause(issuerName,""));
            if (securityKey == null)
            {
                throw new AuthenticationException(string.Format("No security key for issuer '{0}' could be found.", issuerName));
            }
            
            return securityKey;
        }
    }
}
