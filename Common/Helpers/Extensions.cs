﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public static class Extensions
    {
        public static TResult With<TInput, TResult>(this TInput o, Func<TInput, TResult> evaluator)
            where TResult : class
            where TInput : class
        {
            Contract.Requires(evaluator != null);

            if (o == null)
            {
                return null;
            }

            return evaluator(o);
        }

        public static TInput If<TInput>(this TInput o, Func<TInput, bool> evaluator) where TInput : class
        {
            Contract.Requires(evaluator != null);

            if (o == null)
            {
                return null;
            }

            return evaluator(o) ? o : null;
        }
    }
}
