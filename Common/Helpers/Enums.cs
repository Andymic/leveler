﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public enum UserType
    {
        None = 0,
        Personal = 1,
        Student = 2,
        Landlord = 3,
        Business = 4
    }

    public enum Utilities
    {
        None = 0,
        Refrigerator = 1,
        Stove = 2,
        Washer = 3,
        Dryer = 4,
        DishWasher = 5,
        Microwave = 6
    }

    public enum ContractStatus
    {
        Expired = 0,
        Active = 1,
        Inactive = 2,
        Sealed = 3
    }
}
