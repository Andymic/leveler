﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace Common.Helpers
{
    public static class AcceptMediaTypes
    {
        public const string ApplicationJson = "application/json";

        public const string ApplicationXml = "application/xml";

        public const string ApplicationPdf = "application/pdf";
    }

    public class RestServiceHelper
    {
        #region Get
        public static async Task<TResponse> ServiceGetAsync<TResponse>(Uri url, string controller, string accept = AcceptMediaTypes.ApplicationXml)
        {
            return await GetAsync<TResponse>(string.Format("{0}{1}", url, controller), accept).ConfigureAwait(false);
        }

        private static async Task<TResponse> GetAsync<TResponse>(string requestUri, string accept = AcceptMediaTypes.ApplicationXml)
        {
            HttpResponseMessage serviceResponse = await Client(requestUri, accept).GetAsync(requestUri).ConfigureAwait(false);
            return await serviceResponse.Content.ReadAsAsync<TResponse>().ConfigureAwait(false);
        }
        #endregion

        #region Put
        public static async Task<TResponse> ServicePutAsync<TResponse>(Uri url, string controller, string accept = AcceptMediaTypes.ApplicationXml)
        {
            return await PutAsync<TResponse>(string.Format("{0}{1}", url, controller), accept).ConfigureAwait(false);
        }

        public static async Task<TResponse> ServicePutAsync<TResponse, TRequest>(TRequest request, Uri url, string controller, string accept = AcceptMediaTypes.ApplicationXml)
        {
            return await PutAsync<TResponse, TRequest>(request, string.Format("{0}{1}", url, controller), accept).ConfigureAwait(false);
        }

        public static async Task ServicePutAsync(Uri url, string controller, string accept = AcceptMediaTypes.ApplicationXml)
        {
            await PutAsync(string.Format("{0}{1}", url, controller), accept).ConfigureAwait(false);
        }
 
        public static async Task ServicePutAsync<TRequest>(TRequest request, Uri url, string controller, string accept = AcceptMediaTypes.ApplicationXml)
        {
            await PutAsync(request, string.Format("{0}{1}", url, controller), accept).ConfigureAwait(false);
        }

        private static async Task<TResponse> PutAsync<TResponse, TRequest>(TRequest request, string requestUri, string accept = AcceptMediaTypes.ApplicationXml)
        {
            HttpResponseMessage serviceResponse = await Client(requestUri, accept).PutAsJsonAsync(requestUri, request).ConfigureAwait(false);
            return await serviceResponse.Content.ReadAsAsync<TResponse>().ConfigureAwait(false);
        }

        private static async Task PutAsync<TRequest>(TRequest request, string requestUri, string accept = AcceptMediaTypes.ApplicationXml)
        {
            HttpResponseMessage serviceResponse = await Client(requestUri, accept).PutAsJsonAsync(requestUri, request).ConfigureAwait(false);
        }

        private static async Task<TResponse> PutAsync<TResponse>(string requestUri, string accept = AcceptMediaTypes.ApplicationXml)
        {
            HttpResponseMessage serviceResponse = await Client(requestUri, accept).PutAsync(requestUri, new StringContent(string.Empty)).ConfigureAwait(false);
            return await serviceResponse.Content.ReadAsAsync<TResponse>().ConfigureAwait(false);
        }

        private static async Task PutAsync(string requestUri, string accept = AcceptMediaTypes.ApplicationXml)
        {
            HttpResponseMessage serviceResponse = await Client(requestUri, accept).PutAsync(requestUri, new StringContent(string.Empty)).ConfigureAwait(false);
        }
        #endregion

        #region Post
        public static async Task<TResponse> ServicePostAsync<TResponse, TRequest>(TRequest request, Uri url, string controller, string accept = AcceptMediaTypes.ApplicationXml)
        {
            return await PostAsync<TResponse, TRequest>(request, string.Format("{0}{1}", url, controller), accept).ConfigureAwait(false);
        }

        public static async Task ServicePostAsync(Uri url, string controller, HttpContent content, string accept = AcceptMediaTypes.ApplicationXml)
        {
            await PostAsync(string.Format("{0}{1}", url, controller), content, accept).ConfigureAwait(false);
        }

        private static async Task PostAsync(string requestUri, HttpContent content, string accept = AcceptMediaTypes.ApplicationXml)
        {
            HttpResponseMessage serviceResponse =
               await Client(requestUri, accept).SendAsync(new HttpRequestMessage(new HttpMethod("POST"), requestUri)
               {
                   Content = content
               });
        }

        private static async Task<TResponse> PostAsync<TResponse, TRequest>(TRequest request, string requestUri, string accept = AcceptMediaTypes.ApplicationXml)
        {
            HttpResponseMessage serviceResponse = await Client(requestUri, accept).PostAsJsonAsync(requestUri, request).ConfigureAwait(false);
            return await serviceResponse.Content.ReadAsAsync<TResponse>().ConfigureAwait(false);
        }
        #endregion

        #region Delete
        public static async Task ServiceDeleteAsync(Uri url, int id, string controller, string accept = AcceptMediaTypes.ApplicationXml)
        {
            await DeleteAsync(string.Format("{0}{1}/?id=" + id, url, controller), accept).ConfigureAwait(false);
        }

        private static async Task DeleteAsync(string requestUri, string accept = AcceptMediaTypes.ApplicationXml)
        {
            HttpResponseMessage serviceResponse = await Client(requestUri, accept).DeleteAsync(requestUri).ConfigureAwait(false);
        }
        #endregion

        private static HttpClient Client(string requestUri, string accept = AcceptMediaTypes.ApplicationJson)
        {
            if (requestUri == null)
            {
                throw new ArgumentNullException("requestUri");
            }

            var client = HttpHelper.GetJwtHttpClient(new Uri(requestUri), accept);

            return client;
        }
    }
}
