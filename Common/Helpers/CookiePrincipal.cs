﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Common.Helpers
{
    /// <summary>
    /// An extension of <see cref="ClaimsPrincipal"/> that includes the cookie collection that the principal was created from.
    /// </summary>
    [Serializable]
    public class CookiePrincipal : ClaimsPrincipal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CookiePrincipal"/> class.
        /// </summary>
        /// <param name="cookies"></param>
        public CookiePrincipal(HttpCookieCollection cookies)
            : base()
        {
            Contract.Requires(cookies != null);
            this.TokenCookies = cookies;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CookiePrincipal"/> class.
        /// </summary>
        /// <param name="cookies"></param>
        /// <param name="identities"></param>
        public CookiePrincipal(HttpCookieCollection cookies, IEnumerable<ClaimsIdentity> identities)
            : base(identities)
        {
            Contract.Requires(cookies != null);
            this.TokenCookies = cookies;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CookiePrincipal"/> class.
        /// </summary>
        /// <param name="cookies"></param>
        /// <param name="identity"></param>
        public CookiePrincipal(HttpCookieCollection cookies, IIdentity identity)
            : base(identity)
        {
            Contract.Requires(cookies != null);
            this.TokenCookies = cookies;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CookiePrincipal"/> class.
        /// </summary>
        /// <param name="cookies"></param>
        /// <param name="principal"></param>
        public CookiePrincipal(HttpCookieCollection cookies, IPrincipal principal)
            : base(principal)
        {
            Contract.Requires(cookies != null);
            this.TokenCookies = cookies;
        }

        /// <summary>
        /// Gets the cookie collection that contains the token cookies that the principal was created from.
        /// </summary>
        public HttpCookieCollection TokenCookies { get; private set; }

        [ContractInvariantMethod]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Required for code contracts.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "*", Justification = "Ignore Code Contract elements for StyleCop.")]
        private void ObjectInvariant()
        {
            Contract.Invariant(this.TokenCookies != null);
        }
    }
}
