﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model.DealModel;

namespace Leveler.Business.Managers.Interfaces
{
    public interface IDealManager
    {
        List<DealDTO> Get();
        DealDTO Get(object id);
        DealDTO Get(string company);
        bool Exists(DealDTO entity);
        void Add(DealDTO entity);
        void Update(DealDTO entity);
        void Delete(int id);
    }
}
