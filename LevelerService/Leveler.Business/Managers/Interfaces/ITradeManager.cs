﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model.TradeModel;

namespace Leveler.Business.Managers.Interfaces
{
    public interface ITradeManager
    {

        List<TradeDTO> Get();
        TradeDTO Get(object id);
        TradeDTO Get(string username);
    }
}
