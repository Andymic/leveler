﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model.BillModel;

namespace Leveler.Business.Managers.Interfaces
{
    public interface IBillManager
    {
        List<BillDTO> Get();
        BillDTO Get(object id);
        BillDTO Get(string username);
        bool Exists(BillDTO entity);
        void Add(BillDTO entity);
        void Update(BillDTO entity);
        void Delete(int id);
    }
}
