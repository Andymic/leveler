﻿using Common.Model;
using Common.Model.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Business.Managers
{
    public interface IUserExternalLoginManager
    {
        IEnumerable<UserExternalLoginDTO> Get();
        UserExternalLoginDTO Get(int Id);
        bool Exists(UserExternalLoginDTO entity);
        void Add(UserExternalLoginDTO entity);
        void Update(UserExternalLoginDTO entity);
        void Delete(int id);
    }
}
