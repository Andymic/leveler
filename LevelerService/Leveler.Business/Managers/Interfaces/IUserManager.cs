﻿using Common.Model;
using Common.Model.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Business.Managers
{
    public interface IUserManager
    {
        List<UserDTO> Get();
        UserDTO Get(object id);
        UserDTO Get(string username);
        UserDTO Get(UserExternalLoginDTO loginInfo);
        UserDTO AddExternalLogin(UserExternalLoginDTO loginInfo);
        bool Exists(UserDTO entity);
        void Add(UserDTO entity);
        void Update(UserDTO entity);
        void Delete(int id);
    }
}
