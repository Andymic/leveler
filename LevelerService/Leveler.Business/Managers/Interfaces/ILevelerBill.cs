﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model.BillModel;

namespace Leveler.Business.Managers.Interfaces
{
    public interface ILevelerBill
    {
        List<BillDTO> Get();
        BillDTO Get(object id);
        BillDTO Get(string username);
    }
}
