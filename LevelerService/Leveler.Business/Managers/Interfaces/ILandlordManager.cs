﻿using Common.Model.HouseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Business.Managers.Interfaces
{
    public interface ILandlordManager
    {
        IEnumerable<LandlordDTO> Get();
        LandlordDTO Get(object id);
        LandlordDTO Get(string username);
        bool Exists(LandlordDTO entity);
        void Add(LandlordDTO entity);
        void Update(LandlordDTO entity);
        void Delete(int id);
    }
}
