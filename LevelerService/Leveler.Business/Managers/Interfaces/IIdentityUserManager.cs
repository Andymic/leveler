﻿using Common.Model;
using Common.Model.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Business.Managers.Interfaces
{
    public interface IIdentityUserManager
    {
        List<IdentityUserDTO> Get();
        IdentityUserDTO Get(object id);
        IdentityUserDTO Get(string IdentityUsername);
        IdentityUserDTO Get(UserExternalLoginDTO loginInfo);
        IdentityUserDTO AddExternalLogin(UserExternalLoginDTO loginInfo);
        bool Exists(IdentityUserDTO entity);
        void Add(IdentityUserDTO entity);
        void Update(IdentityUserDTO entity);
        void Delete(int id);
    }
}
