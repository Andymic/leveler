﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model.HouseModel;

namespace Leveler.Business.Managers
{
    public interface ILevelerHouse
    {
        List<HouseDTO> Get();
        HouseDTO Get(object id);
        HouseDTO Get(string username);
        bool Exists(HouseDTO entity);
        void Add(HouseDTO entity);
        void Update(HouseDTO entity);
        void Delete(int id);
    }
}
