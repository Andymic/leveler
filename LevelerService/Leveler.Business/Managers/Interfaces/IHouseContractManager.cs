﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model.HouseModel;

namespace Leveler.Business.Managers.Interfaces
{
    public interface IHouseContractManager
    {
        List<HouseContractDTO> Get();
        HouseContractDTO Get(object id);
        HouseContractDTO Get(string username);
        bool Exists(HouseContractDTO entity);
        void Add(HouseContractDTO entity);
        void Update(HouseContractDTO entity);
        void Delete(int id);
    }
}
