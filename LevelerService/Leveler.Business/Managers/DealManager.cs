﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Domain.POCO;
using Common.Model.DealModel;
using Leveler.Data.Repositories.Interfaces;
using Leveler.Business.Managers.Interfaces;

namespace Leveler.Business.Managers
{
    public class DealManager: IDealManager
    {
        private readonly IDealRepository dealRepo;

        public DealManager(IDealRepository _dealRepo)
        {
            this.dealRepo = _dealRepo;

        }

        public List<DealDTO> Get()
        {
            var entities = dealRepo.Get().ToList();
            List<DealDTO> dtos = MapDeal(entities);

            return dtos;
        }

        public DealDTO Get(object id)
        {
            var entity = dealRepo.Get(id);

            return MapDeal(entity);
        }

        public DealDTO Get(string company)
        {
            var entity = dealRepo.Get().FirstOrDefault(u =>u.Company == company);

            return MapDeal(entity);
        }


        private DealDTO MapDeal(Deal entity)
        {
            if (entity == null)
                return null;

            return new DealDTO
            {
                HouseId = entity.HouseId,
                Company = entity.Company,
                Category = entity.Category,
                Description = entity.Description,
                Item = entity.Item,
                Expire = entity.Expire,
                Status = entity.Status,
                Id=entity.Id
            };
        }

        private Deal MapDeal(DealDTO entity)
        {
            if (entity == null)
                return null;

            return new Deal
            {
                HouseId = entity.HouseId,
               Company= entity.Company,
               Category=entity.Category,
               Description=entity.Description,
               Item=entity.Item,
               Expire=entity.Expire,
               Status=entity.Status
            };
        }

        private Deal MapDeal(Deal poco, DealDTO entity)
        {
            if (poco == null || entity == null)
                return null;

            poco.HouseId = entity.HouseId;
               poco.Company= entity.Company;
               poco.Category=entity.Category;
               poco.Description=entity.Description;
               poco.Item=entity.Item;
              poco.Expire=entity.Expire;
              poco.Status = entity.Status;
            return poco;
        }

        private List<DealDTO> MapDeal(List<Deal> entities)
        {
            if (entities == null || entities.Count() == 0)
                return new List<DealDTO>();

            List<DealDTO> dtos = new List<DealDTO>();

            foreach (var entity in entities)
            {
                dtos.Add(MapDeal(entity));
            }

            return dtos;
        }

        public bool Exists(DealDTO entity)
        {
            return dealRepo.Exists(MapDeal(entity));
        }

        public void Add(DealDTO entity)
        {
            dealRepo.Add(MapDeal(entity));
        }

        public void Update(DealDTO entity)
        {
            var dbEntity = dealRepo.Get(entity.Id);

            dbEntity = MapDeal(dbEntity, entity);

            dealRepo.Update(dbEntity);
        }

        public void Delete(int id)
        {
            var dbentity = dealRepo.Get(id);

            dealRepo.Delete(dbentity);
        }
    }
}

