﻿using Leveler.Business.Managers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model.HouseModel;
using Leveler.Data.Repositories.Interfaces;

namespace Leveler.Business.Managers
{
    public class LandlordManager : ILandlordManager
    {
        private readonly ILandlordRepository LandlordRepo;

        public LandlordManager(ILandlordRepository _landlordRepo)
        {
            this.LandlordRepo = _landlordRepo;
        }

        public IEnumerable<LandlordDTO> Get()
        {
            var entities = LandlordRepo.Get();

            return entities.AsEnumerable().toDTO();
        }

        public LandlordDTO Get(object id)
        {
            var entity = LandlordRepo.Get(id);

            return entity.toDTO();
        }

        public LandlordDTO Get(string username)
        {
            throw new NotImplementedException();
        }

        public bool Exists(LandlordDTO entity)
        {
            return LandlordRepo.Exists(entity.toPOCO());
        }

        public void Add(LandlordDTO entity)
        {
            LandlordRepo.Add(entity.toPOCO());
        }

        public void Update(LandlordDTO entity)
        {
            var dbentity = LandlordRepo.Get(entity.Id);

            dbentity = entity.toUpdatedPOCO(dbentity);

            LandlordRepo.Update(dbentity);
            
        }

        public void Delete(int id)
        {
            LandlordRepo.Delete(LandlordRepo.Get(id));
        }
    }
}
