﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Data.Repositories.Interfaces;
using Leveler.Domain.POCO;
using Common.Model.HouseModel;


namespace Leveler.Business.Managers
{
    public class LevelerHouse :ILevelerHouse
    {
        private readonly IHouseRepository HouseRepo;

        public LevelerHouse(IHouseRepository _houseRepo)
        {
            this.HouseRepo = _houseRepo;
        }

        public List<HouseDTO> Get()
        {
            var entities = HouseRepo.Get().ToList();
            List<HouseDTO> dtos = MapHouse(entities);

            return dtos;
        }

        public HouseDTO Get(object id)
        {
            var entity = HouseRepo.Get(id);

            return MapHouse(entity);
        }

        public HouseDTO Get(string username)
        {
            var entity = HouseRepo.Get().FirstOrDefault(u => u.UserName == username);

            return MapHouse(entity);
        }

        public bool Exists(HouseDTO entity)
        {
            return HouseRepo.Exists(MapHouse(entity));
        }

        public void Add(HouseDTO entity)
        {
            HouseRepo.Add(MapHouse(entity));
        }

        public void Update(HouseDTO entity)
        {
            var dbEntity = HouseRepo.Get(entity.Id);

            dbEntity = MapHouse(dbEntity, entity);

            HouseRepo.Update(dbEntity);
        }

        public void Delete(int id)
        {
            var dbentity = HouseRepo.Get(id);

            HouseRepo.Delete(dbentity);
        }

        private HouseDTO MapHouse(House entity)
        {
            if (entity == null)
                return null;

            return new HouseDTO
            {
                Owner = entity.Owner,
                Location = entity.Location,
                Id = entity.Id,
                UserName = entity.UserName
            };
        }

        private House MapHouse(HouseDTO entity)
        {
            if (entity == null)
                return null;

            return new House
            {
                Owner = entity.Owner,
                Location = entity.Location,
                Id = entity.Id,
                UserName = entity.UserName
            };
        }

        private House MapHouse(House poco, HouseDTO entity)
        {
            if (entity == null || entity == null)
                return null;

            poco.UserName = entity.UserName;
            poco.Location = entity.Location;
            poco.Owner = entity.Owner;
            poco.Id = poco.Id;
            return poco;
        }

        private List<HouseDTO> MapHouse(List<House> entities)
        {
            if (entities == null || entities.Count() == 0)
                return new List<HouseDTO>();

            List<HouseDTO> dtos = new List<HouseDTO>();

            foreach (var entity in entities)
            {
                dtos.Add(MapHouse(entity));
            }

            return dtos;
        }
    }
}
