﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Data.Repositories.Interfaces;
using Leveler.Domain.POCO;
using Common.Model.HouseModel;
using Leveler.Business.Managers.Interfaces;

namespace Leveler.Business.Managers
{
    public class HouseContractManager : IHouseContractManager
    {
         private readonly IHouseContractRepository HouseContractRepo;

         public HouseContractManager(IHouseContractRepository _HouseContractRepo)
        {
            this.HouseContractRepo = _HouseContractRepo;
        }

        public List<HouseContractDTO> Get()
        {
            var entities = HouseContractRepo.Get();

            return entities.toDTO().ToList();
        }

        public HouseContractDTO Get(object id)
        {
            var entity = HouseContractRepo.Get(id);

            return entity.toDTO();
        }

        public HouseContractDTO Get(string username)
        {
            var entity = HouseContractRepo.FirstOrDefault(u => u.Landlord.User.UserName == username);

            return entity.toDTO();
        }

        public bool Exists(HouseContractDTO entity)
        {
            return HouseContractRepo.Exists(entity.toPOCO());
        }

        public void Add(HouseContractDTO entity)
        {
            HouseContractRepo.Add(entity.toPOCO());
        }

        public void Update(HouseContractDTO entity)
        {
            var dbEntity = HouseContractRepo.Get(entity.Id);

            dbEntity = entity.toUpdatedPOCO(dbEntity);

            HouseContractRepo.Update(dbEntity);
        }

        public void Delete(int id)
        {
            var dbentity = HouseContractRepo.Get(id);

            HouseContractRepo.Delete(dbentity);
        }
    }
}
