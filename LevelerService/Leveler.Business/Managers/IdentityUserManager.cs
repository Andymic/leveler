﻿using Common.Model;
using Common.Model.UserManagement;
using Leveler.Business.Managers.Interfaces;
using Leveler.Data.Repositories;
using Leveler.Domain.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Business.Managers
{
    public class IdentityUserManager:IIdentityUserManager
    {
        private readonly IUserRepository IdentityUserRepo;

        public IdentityUserManager(IUserRepository _IdentityUserRepo)
        {
            this.IdentityUserRepo = _IdentityUserRepo;
        }

        public List<IdentityUserDTO> Get()
        {
            var entities = IdentityUserRepo.Get().ToList();
            List<IdentityUserDTO> dtos = MapIdentityUsers(entities);

            return dtos;
        }

        public IdentityUserDTO Get(object id)
        {
            var entity = IdentityUserRepo.Get(id);

            return MapIdentityUser(entity);
        }

        public IdentityUserDTO Get(string email)
        {
            var entity = IdentityUserRepo.Get().FirstOrDefault(u => u.Email == email);

            return MapIdentityUser(entity);
        }

        public IdentityUserDTO Get(UserExternalLoginDTO loginInfo)
        {
            var IdentityUser = IdentityUserRepo.FirstOrDefault(u => u.ExternalLogins != null && u.ExternalLogins.Any(l => l.LoginProvider == loginInfo.LoginProvider && l.ProviderKey == loginInfo.ProviderKey));

            return MapIdentityUser(IdentityUser);
        }

        public bool Exists(IdentityUserDTO entity)
        {
            return IdentityUserRepo.Exists(MapIdentityUser(entity));
        }

        public void Add(IdentityUserDTO entity)
        {
            entity.DateCreated = DateTime.Now;
            entity.LastLogin = DateTime.Now;
            IdentityUserRepo.Add(MapIdentityUser(entity));
        }

        public void Update(IdentityUserDTO entity)
        {
            var dbEntity = IdentityUserRepo.Get(entity.Id);

            dbEntity = MapIdentityUser(dbEntity, entity);

            IdentityUserRepo.Update(dbEntity);
        }

        public void Delete(int id)
        {
            var dbentity = IdentityUserRepo.Get(id);

            IdentityUserRepo.Delete(dbentity);
        }

        private IdentityUserDTO MapIdentityUser(User entity)
        {
            if (entity == null)
                return null;

            return new IdentityUserDTO
            {
                DateCreated = entity.DateCreated,
                Email = entity.Email,
                Id = entity.Id,
                LastLogin = entity.LastLogin,
                UserName = entity.UserName,
                Password = entity.Password,
                UserType = entity.UserType
            };
        }

        private User MapIdentityUser(IdentityUserDTO entity)
        {
            if (entity == null)
                return null;

            return new User
            {
                DateCreated = entity.DateCreated,
                Email = entity.Email,
                Id = entity.Id,
                LastLogin = entity.LastLogin,
                UserName = entity.UserName,
                Password = entity.Password,
                UserType = entity.UserType
            };
        }

        private User MapIdentityUser(User poco, IdentityUserDTO entity)
        {
            if (entity == null || entity == null)
                return null;

            poco.DateCreated = entity.DateCreated;
            poco.Email = entity.Email;
            poco.Id = entity.Id;
            poco.LastLogin = entity.LastLogin;
            poco.UserName = entity.UserName;
            poco.ExternalLogins = new List<UserExternalLogin>();
            poco.Password = entity.Password;
            poco.UserType = entity.UserType;

            return poco;
        }

        private List<IdentityUserDTO> MapIdentityUsers(List<User> entities)
        {
            if (entities == null || entities.Count() == 0)
                return new List<IdentityUserDTO>();

            List<IdentityUserDTO> dtos = new List<IdentityUserDTO>();

            foreach (var entity in entities)
            {
                dtos.Add(MapIdentityUser(entity));
            }

            return dtos;
        }

        public IdentityUserDTO AddExternalLogin(UserExternalLoginDTO loginInfo)
        {
            var IdentityUser = IdentityUserRepo.Get(loginInfo.UserId);
            if (IdentityUser.ExternalLogins == null)
                IdentityUser.ExternalLogins = new List<UserExternalLogin>();
            IdentityUser.ExternalLogins.Add(MapLogin(loginInfo));
            IdentityUserRepo.Update(IdentityUser);
            return MapIdentityUser(IdentityUser);
        }

        private UserExternalLogin MapLogin(UserExternalLoginDTO login)
        {
            return new UserExternalLogin()
            {
                LoginProvider = login.LoginProvider,
                ProviderKey = login.ProviderKey,
                UserId = login.UserId
            };
        }
    }
}
