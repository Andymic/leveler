﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model.UserManagement;
using Leveler.Data.Repositories;
using Leveler.Domain.POCO;

namespace Leveler.Business.Managers
{
    public class UserExternalLoginManager:IUserExternalLoginManager
    {
        private readonly IUserExternalLoginRepository UserExternalLoginRepo;

        public UserExternalLoginManager(IUserExternalLoginRepository _userExternalLoginRepo)
        {
            this.UserExternalLoginRepo = _userExternalLoginRepo;
        }

        public IEnumerable<UserExternalLoginDTO> Get()
        {
            var pocos = UserExternalLoginRepo.Get();

            return MapLogins(pocos);
        }

        public UserExternalLoginDTO Get(int Id)
        {
            var login = UserExternalLoginRepo.Get(Id);

            return MapLogin(login);
        }

        public bool Exists(UserExternalLoginDTO entity)
        {
            throw new NotImplementedException();
        }

        public void Add(UserExternalLoginDTO entity)
        {
            throw new NotImplementedException();
        }

        public void Update(UserExternalLoginDTO entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        private UserExternalLoginDTO MapLogin(UserExternalLogin login)
        {
            return new UserExternalLoginDTO(login.LoginProvider, login.ProviderKey, login.UserId);
        }

        private UserExternalLogin MapLogin(UserExternalLoginDTO login)
        {
            return new UserExternalLogin()
            {
                LoginProvider = login.LoginProvider,
                ProviderKey = login.ProviderKey,
                UserId = login.UserId
            };
        }

        private IEnumerable<UserExternalLoginDTO> MapLogins(IEnumerable<UserExternalLogin> logins)
        {
            List<UserExternalLoginDTO> dtos = new List<UserExternalLoginDTO>();

            foreach (var login in logins)
                dtos.Add(new UserExternalLoginDTO(login.LoginProvider, login.ProviderKey, login.UserId));

            return dtos;
        }
    }
}
