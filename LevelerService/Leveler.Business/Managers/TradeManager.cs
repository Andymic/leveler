﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Domain.POCO;
using Common.Model.TradeModel;
using Leveler.Data.Repositories.Interfaces;
using Leveler.Business.Managers.Interfaces;

namespace Leveler.Business.Managers
{
    public class TradeManager: ITradeManager
    {
        private readonly ITradeRepository TradeRepo;

         public TradeManager(ITradeRepository _tradeRepo)
        {
            this.TradeRepo = _tradeRepo;
        }

        public List<TradeDTO> Get()
        {
            var entities = TradeRepo.Get().ToList();
            List<TradeDTO> dtos = MapTrade(entities);

            return dtos;
        }

        public TradeDTO Get(object id)
        {
            var entity = TradeRepo.Get(id);

            return MapTrade(entity);
        }

        public TradeDTO Get(string Item)
        {
            var entity = TradeRepo.Get().FirstOrDefault(u => u.Item == Item);

            return MapTrade(entity);
        }


        private TradeDTO MapTrade(Trade entity)
        {
            if (entity == null)
                return null;

            return new TradeDTO
            {
               Id=entity.Id,
               Category=entity.Category,
               Active=entity.Active,
               Description=entity.Description,
               Item=entity.Item
            };
        }

        private Trade MapTrade(TradeDTO entity)
        {
            if (entity == null)
                return null;

            return new Trade
            {
                Id = entity.Id,
                Category = entity.Category,
                Active = entity.Active,
                Description = entity.Description,
                Item = entity.Item
            };
        }

        private Trade MapTrade(Trade poco, TradeDTO entity)
        {
            if (entity == null || entity == null)
                return null;

            poco.Id = entity.Id;
            poco.Category = entity.Category;
            poco.Active = entity.Active;
            poco.Description = entity.Description;
            poco.Item = entity.Item;
            return poco;
        }

        private List<TradeDTO> MapTrade(List<Trade> entities)
        {
            if (entities == null || entities.Count() == 0)
                return new List<TradeDTO>();

            List<TradeDTO> dtos = new List<TradeDTO>();

            foreach (var entity in entities)
            {
                dtos.Add(MapTrade(entity));
            }

            return dtos;
        }
    }
}
