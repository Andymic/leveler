﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Data.Repositories;
using Leveler.Domain.POCO;
using Common.Model.UserManagement;
using Common.Helpers;

namespace Leveler.Business.Managers
{
    public class UserManager:IUserManager
    {
        private readonly IUserRepository UserRepo;

        public UserManager(IUserRepository _userRepo)
        {
            this.UserRepo = _userRepo;
        }

        public List<UserDTO> Get()
        {
            var entities = UserRepo.Get().ToList();
            List<UserDTO> dtos = MapUsers(entities);

            return dtos;
        }

        public UserDTO Get(object id)
        {
            var entity = UserRepo.Get(id);

            return MapUser(entity);
        }

        public UserDTO Get(string email)
        {
            var entity = UserRepo.Get().FirstOrDefault(u => u.Email == email);

            return MapUser(entity);
        }

        public UserDTO Get(UserExternalLoginDTO loginInfo)
        {
            var user = UserRepo.FirstOrDefault(u => u.ExternalLogins != null && u.ExternalLogins.Any(l => l.LoginProvider == loginInfo.LoginProvider && l.ProviderKey == loginInfo.ProviderKey));

            return MapUser(user);
        }

        public bool Exists(UserDTO entity)
        {
            return UserRepo.Exists(MapUser(entity));
        }

        public void Add(UserDTO entity)
        {
            entity.DateCreated = DateTime.Now;
            entity.LastLogin = DateTime.Now;
            UserRepo.Add(MapUser(entity));
        }

        public void Update(UserDTO entity)
        {
            var dbEntity = UserRepo.Get(entity.Id);

            dbEntity = MapUser(dbEntity, entity);

            UserRepo.Update(dbEntity);
        }

        public void Delete(int id)
        {
            var dbentity = UserRepo.Get(id);

            UserRepo.Delete(dbentity);
        }

        private UserDTO MapUser(User entity)
        {
            if (entity == null)
                return null;

            return new UserDTO
            {
                DateCreated = entity.DateCreated,
                Email = entity.Email,
                Id = entity.Id,
                LastLogin = entity.LastLogin,
                UserName = entity.UserName,
                UserType=entity.UserType
            };
        }

        private User MapUser(UserDTO entity)
        {
            if (entity == null)
                return null;

            return new User
            {
                DateCreated = entity.DateCreated,
                Email = entity.Email,
                Id = entity.Id,
                LastLogin = entity.LastLogin,
                UserName = entity.UserName,
                UserType=entity.UserType
            };
        }

        private User MapUser(User poco, UserDTO entity)
        {
            if (entity == null || entity == null)
                return null;
            poco.UserType = entity.UserType;
            poco.DateCreated = entity.DateCreated;
            poco.Email = entity.Email;
            poco.Id = entity.Id;
            poco.LastLogin = entity.LastLogin;
            poco.UserName = entity.UserName;
            poco.ExternalLogins = new List<UserExternalLogin>();

            return poco;
        }

        private List<UserDTO> MapUsers(List<User> entities)
        {
            if (entities == null || entities.Count() == 0)
                return new List<UserDTO>();

            List<UserDTO> dtos = new List<UserDTO>();

            foreach (var entity in entities)
            {
                dtos.Add(MapUser(entity));
            }

            return dtos;
        }

        public UserDTO AddExternalLogin(UserExternalLoginDTO loginInfo)
        {
            var user = UserRepo.Get(loginInfo.UserId);
            if (user.ExternalLogins == null)
                user.ExternalLogins = new List<UserExternalLogin>();
            user.ExternalLogins.Add(MapLogin(loginInfo));
            UserRepo.Update(user);
            return MapUser(user);
        }

        private UserExternalLogin MapLogin(UserExternalLoginDTO login)
        {
            return new UserExternalLogin()
            {
                LoginProvider = login.LoginProvider,
                ProviderKey = login.ProviderKey,
                UserId = login.UserId
            };
        }
    }
}
