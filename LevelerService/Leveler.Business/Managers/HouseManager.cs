﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Data.Repositories.Interfaces;
using Leveler.Domain.POCO;
using Common.Model.HouseModel;


namespace Leveler.Business.Managers
{
    public class HouseManager :IHouseManager
    {
        private readonly IHouseRepository HouseRepo;

        public HouseManager(IHouseRepository _houseRepo)
        {
            this.HouseRepo = _houseRepo;
        }

        public List<HouseDTO> Get()
        {
            var entities = HouseRepo.Get();

            return entities.toDTO().ToList();
        }

        public HouseDTO Get(object id)
        {
            var entity = HouseRepo.Get(id);

            return entity.toDTO();
        }

        public HouseDTO Get(string username)
        {
            var entity = HouseRepo.FirstOrDefault(u => u.Landlord.User.UserName == username);

            return entity.toDTO();
        }

        public bool Exists(HouseDTO entity)
        {
            return HouseRepo.Exists(entity.toPOCO());
        }

        public void Add(HouseDTO entity)
        {
            HouseRepo.Add(entity.toPOCO());
        }

        public void Update(HouseDTO entity)
        {
            var dbEntity = HouseRepo.Get(entity.Id);

            dbEntity = entity.toUpdatedPOCO(dbEntity);

            HouseRepo.Update(dbEntity);
        }

        public void Delete(int id)
        {
            var dbentity = HouseRepo.Get(id);

            HouseRepo.Delete(dbentity);
        }
    }
}
