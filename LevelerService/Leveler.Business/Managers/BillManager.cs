﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Domain.POCO;
using Common.Model.BillModel;
using Leveler.Data.Repositories.Interfaces;
using Leveler.Business.Managers.Interfaces;

namespace Leveler.Business.Managers
{
    public class BillManager : IBillManager
    {
        private readonly IBillRepository BillRepo;

        public BillManager(IBillRepository _billRepo)
        {
            this.BillRepo = _billRepo;
  
        }

        public List<BillDTO> Get()
        {
            var entities = BillRepo.Get().ToList();
            List<BillDTO> dtos = MapBill(entities);

            return dtos;
        }

        public BillDTO Get(object id)
        {
            var entity = BillRepo.Get(id);

            return MapBill(entity);
        }

        public BillDTO Get(string nickname)
        {
            var entity = BillRepo.Get().FirstOrDefault(u => u.Nickname == nickname);

            return MapBill(entity);
        }


        private BillDTO MapBill(Bill entity)
        {
            if (entity == null)
                return null;

            return new BillDTO
            {
               HouseId=entity.HouseId,
               Amount=entity.Amount,
               Reminder=entity.Reminder,
               Status=entity.Status,
               Nickname=entity.Nickname,
               Id=entity.Id,
               UserId=entity.UserId,
               DueDate=entity.DueDate
            };
        }

        private Bill MapBill(BillDTO entity)
        {
            if (entity == null)
                return null;

            return new Bill
            {
                HouseId = entity.HouseId,
                Amount = entity.Amount,
                Reminder = entity.Reminder,
                Status = entity.Status,
                Nickname = entity.Nickname,
                Id = entity.Id,
                UserId = entity.UserId,
                DueDate = entity.DueDate
            };
        }

        private Bill MapBill(Bill poco, BillDTO entity)
        {
            if (entity == null || entity == null)
                return null;

            poco.HouseId = entity.HouseId;
            poco.Amount = entity.Amount;
            poco.Reminder = entity.Reminder;
            poco.Status = entity.Status;
            poco.Nickname = entity.Nickname;
            poco.UserId = entity.UserId;
            poco.DueDate = entity.DueDate;
            poco.Id = entity.Id;
            return poco;
        }

        private List<BillDTO> MapBill(List<Bill> entities)
        {
            if (entities == null || entities.Count() == 0)
                return new List<BillDTO>();

            List<BillDTO> dtos = new List<BillDTO>();

            foreach (var entity in entities)
            {
                dtos.Add(MapBill(entity));
            }

            return dtos;
        }

        public bool Exists(BillDTO entity)
        {
            return BillRepo.Exists(MapBill(entity));
        }

        public void Add(BillDTO entity)
        {
            BillRepo.Add(MapBill(entity));
        }

        public void Update(BillDTO entity)
        {
            var dbEntity = BillRepo.Get(entity.Id);

            dbEntity = MapBill(dbEntity, entity);

            BillRepo.Update(dbEntity);
        }

        public void Delete(int id)
        {
            var dbentity = BillRepo.Get(id);

            BillRepo.Delete(dbentity);
        }
    }
}
