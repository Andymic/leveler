﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model.HouseModel;
using Leveler.Domain.POCO;
using Common.Model;

namespace Leveler.Business
{
    public static class MappingExtensions
    {
        #region LandLor Mapping

        public static LandlordDTO toDTO(this Landlord poco)
        {
            if (poco == null)
                return null;

            return new LandlordDTO
            {
                DateCreated = poco.DateCreated,
                DateModified = poco.DateModified,
                Id = poco.Id,
                UserId = poco.UserId,
                Houses = poco.Houses.toDTO()
            };
        }

        public static Landlord toPOCO(this LandlordDTO dto)
        {
            if (dto == null)
                return null;

            return new Landlord
            {
                DateCreated = dto.DateCreated,
                DateModified = dto.DateModified,
                Id = dto.Id,
                UserId = dto.UserId,
                Houses = dto.Houses.toPOCO()
            };
        }

        public static Landlord toUpdatedPOCO(this LandlordDTO dto, Landlord poco)
        {
            if (dto == null)
                return null;

            poco.DateCreated = dto.DateCreated;
            poco.DateModified = dto.DateModified;
            poco.Id = dto.Id;
            poco.UserId = dto.UserId;
            poco.Houses = dto.Houses.toPOCO();

            return poco;
        }

        public static IEnumerable<LandlordDTO> toDTO(this IEnumerable<Landlord> pocos)
        {
            var dtos = new List<LandlordDTO>();

            foreach (var poco in pocos)
            {
                dtos.Add(poco.toDTO());
            }

            return dtos;
        }

        public static ICollection<Landlord> toPOCO(this IEnumerable<LandlordDTO> dtos)
        {
            var pocos = new List<Landlord>();

            foreach (var dto in dtos)
            {
                pocos.Add(dto.toPOCO());
            }

            return pocos;
        }

        #endregion

        #region House Mapping

        public static HouseDTO toDTO(this House poco)
        {
            if (poco == null)
                return null;

            return new HouseDTO
            {
                DateCreated = poco.DateCreated,
                DateModified = poco.DateModified,
                Id = poco.Id,
                Details = poco.Details,
                LandlordId = poco.LandlordId,
               //Location = poco.Location,
                Utilities = poco.Utilities,
                Tenants = poco.Tenants.toDTO()
            };
        }

        public static House toPOCO(this HouseDTO dto)
        {
            if (dto == null)
                return null;

            return new House
            {
                DateCreated = dto.DateCreated,
                DateModified = dto.DateModified,
                Id = dto.Id,
                Details = dto.Details,
                LandlordId = dto.LandlordId,
                //Location = dto.Location,

                Utilities = dto.Utilities,
            };
        }

        public static House toUpdatedPOCO(this HouseDTO dto, House poco)
        {
            if (dto == null || poco == null)
                return null;

            poco.DateCreated = dto.DateCreated;
            poco.DateModified = dto.DateModified;
            poco.Id = dto.Id;
            poco.Details = dto.Details;
            poco.LandlordId = dto.LandlordId;
           // poco.Location = dto.Location;
            poco.Utilities = dto.Utilities;

            return poco;
        }

        public static IEnumerable<HouseDTO> toDTO(this IEnumerable<House> pocos)
        {
            var dtos = new List<HouseDTO>();

            foreach (var poco in pocos)
            {
                dtos.Add(poco.toDTO());
            }

            return dtos;
        }

        public static ICollection<House> toPOCO(this IEnumerable<HouseDTO> dtos)
        {
            var pocos = new List<House>();

            foreach (var dto in dtos)
            {
                pocos.Add(dto.toPOCO());
            }

            return pocos;
        }

        #endregion

        #region User Mapping

        #region Normal Users

        public static UserDTO toDTO(this User poco)
        {
            if (poco == null)
                return null;

            return new UserDTO
            {
                DateCreated = poco.DateCreated,
                Email = poco.Email,
                Id = poco.Id,
                LastLogin = poco.LastLogin,
                UserName = poco.UserName,
                UserType = poco.UserType,
                DateModified = poco.DateModified,
                HouseId = poco.HouseId,
                LandlordId = poco.LandlordId
            };
        }

        public static User toPOCO(this UserDTO dto)
        {
            if (dto == null)
                return null;

            return new User
            {
                DateCreated = dto.DateCreated,
                Email = dto.Email,
                Id = dto.Id,
                LastLogin = dto.LastLogin,
                UserName = dto.UserName,
                UserType = dto.UserType,
                DateModified = dto.DateModified,
                HouseId = dto.HouseId,
                LandlordId = dto.LandlordId
            };
        }

        public static User toUpdatedPOCO(this UserDTO dto, User poco)
        {
            if (dto == null || poco == null)
                return null;

            poco.DateCreated = dto.DateCreated;
            poco.Email = dto.Email;
            poco.Id = dto.Id;
            poco.LastLogin = dto.LastLogin;
            poco.UserName = dto.UserName;
            poco.UserType = dto.UserType;
            poco.HouseId = dto.HouseId;
            poco.DateModified = dto.DateModified;
            poco.LandlordId = dto.LandlordId;

            return poco;
        }

        public static IEnumerable<UserDTO> toDTO(this IEnumerable<User> pocos)
        {
            var dtos = new List<UserDTO>();

            if (pocos == null)
                return null;

            foreach(var poco in pocos)
            {
                dtos.Add(poco.toDTO());
            }

            return dtos;
        }

        public static ICollection<User> toPOCO(this IEnumerable<UserDTO> dtos)
        {
            var pocos = new List<User>();

            foreach (var dto in dtos)
            {
                pocos.Add(dto.toPOCO());
            }

            return pocos;
        }

        #endregion

        #region Identity Users

        public static IdentityUserDTO toIdentityDTO(this User poco)
        {
            if (poco == null)
                return null;

            return new IdentityUserDTO
            {
                DateCreated = poco.DateCreated,
                Email = poco.Email,
                Id = poco.Id,
                LastLogin = poco.LastLogin,
                UserName = poco.UserName,
                UserType = poco.UserType,
                Password = poco.Password,
                DateModified = poco.DateModified,
                HouseId = poco.HouseId,
                LandlordId = poco.LandlordId
            };
        }

        public static User toPOCO(this IdentityUserDTO dto)
        {
            if (dto == null)
                return null;

            return new User
            {
                DateCreated = dto.DateCreated,
                Email = dto.Email,
                Id = dto.Id,
                LastLogin = dto.LastLogin,
                UserName = dto.UserName,
                UserType = dto.UserType,
                DateModified = dto.DateModified,
                HouseId = dto.HouseId,
                Password = dto.Password,
                LandlordId = dto.LandlordId
            };
        }

        public static User toUpdatedPOCO(this IdentityUserDTO dto, User poco)
        {
            if (dto == null || poco == null)
                return null;

            poco.DateCreated = dto.DateCreated;
            poco.Email = dto.Email;
            poco.Id = dto.Id;
            poco.LastLogin = dto.LastLogin;
            poco.UserName = dto.UserName;
            poco.UserType = dto.UserType;
            poco.HouseId = dto.HouseId;
            poco.DateModified = dto.DateModified;
            poco.Password = dto.Password;
            poco.LandlordId = dto.LandlordId;

            return poco;
        }

        public static IEnumerable<IdentityUserDTO> toIdentityDTO(this IEnumerable<User> pocos)
        {
            var dtos = new List<IdentityUserDTO>();

            foreach (var poco in pocos)
            {
                dtos.Add(poco.toIdentityDTO());
            }

            return dtos;
        }

        public static ICollection<User> toPOCO(this IEnumerable<IdentityUserDTO> dtos)
        {
            var pocos = new List<User>();

            foreach (var dto in dtos)
            {
                pocos.Add(dto.toPOCO());
            }

            return pocos;
        }

        #endregion

        #endregion

        #region HouseContract
        public static HouseContractDTO toDTO(this HouseContract poco)
        {
            if (poco == null)
                return null;

            return new HouseContractDTO
            {
                DateCreated = poco.DateCreated,
                DateModified = poco.DateModified,
                Id = poco.Id,
                LandlordId = poco.LandlordId,
                Tenants = poco.Tenants.toDTO(),
                Status=poco.Status,
                Contract=poco.Contract
            };
        }

        public static HouseContract toPOCO(this HouseContractDTO dto)
        {
            if (dto == null)
                return null;

            return new HouseContract
            {
                DateCreated = dto.DateCreated,
                DateModified = dto.DateModified,
                Id = dto.Id,
                LandlordId = dto.LandlordId,
                Status = dto.Status,
                Contract = dto.Contract
            };
        }

        public static HouseContract toUpdatedPOCO(this HouseContractDTO dto, HouseContract poco)
        {
            if (dto == null || poco == null)
                return null;

            poco.DateCreated = dto.DateCreated;
            poco.DateModified = dto.DateModified;
            poco.Id = dto.Id;
            poco.LandlordId = dto.LandlordId;
            poco.Status = dto.Status;
            poco.Contract = dto.Contract;

            return poco;
        }

        public static IEnumerable<HouseContractDTO> toDTO(this IEnumerable<HouseContract> pocos)
        {
            var dtos = new List<HouseContractDTO>();

            foreach (var poco in pocos)
            {
                dtos.Add(poco.toDTO());
            }

            return dtos;
        }

        public static ICollection<HouseContract> toPOCO(this IEnumerable<HouseContractDTO> dtos)
        {
            var pocos = new List<HouseContract>();

            foreach (var dto in dtos)
            {
                pocos.Add(dto.toPOCO());
            }

            return pocos;
        }
        #endregion HouseContract

        #region Location

        public static LocationDTO toDTO(this Location entity)
        {
            if (entity == null)
                return null;

            return new LocationDTO
            {
                Id = entity.Id,
                DateCreated = entity.DateCreated,
                DateModified = entity.DateModified,
                City = entity.City,
                Latitude = entity.Latitude,
                Longitude = entity.Longitude,
                PostalCode = entity.PostalCode,
                State = entity.State,
                StreetAddress = entity.StreetAddress
            };
        }

        public static Location toPOCO(this LocationDTO entity)
        {
            if (entity == null)
                return null;

            return new Location
            {
                DateCreated = entity.DateCreated,
                DateModified = entity.DateModified,
                City = entity.City,
                Latitude = entity.Latitude,
                Longitude = entity.Longitude,
                PostalCode = entity.PostalCode,
                State = entity.State,
                StreetAddress = entity.StreetAddress
            };
        }

        #endregion
    }
}
