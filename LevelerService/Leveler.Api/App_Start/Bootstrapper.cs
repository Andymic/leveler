﻿using Common.Data;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Leveler.Data;
using Leveler.Data.Repositories;
using Leveler.Data.Repositories.Interfaces;

namespace Leveler.App_Start
{
    public class Bootstrapper
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            container.RegisterType<DataContext, DataContext>(new HierarchicalLifetimeManager());
            container.RegisterType<DataContext, DataContext>(new HierarchicalLifetimeManager());

            container.RegisterType(typeof(IBaseRepository<>), typeof(BaseRepository<,>));
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IHouseRepository, HouseRepository>();
            container.RegisterType<IBillRepository, BillRepository>();
            container.RegisterType<ITradeRepository, TradeRepository>();
            container.RegisterType<IDealRepository, DealRepository>();
            container.RegisterType<IHouseContractRepository, HouseContractRepository>();
            return container;
        }
    }
}