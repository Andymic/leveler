﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Leveler.Business.Managers;
using Leveler.Domain.POCO;
using Common.Model.UserManagement;

namespace Leveler.Controllers
{
    public class UserController : ApiController
    {
        private readonly IUserManager UserManager;

        public UserController(IUserManager _userMngr)
        {
            this.UserManager = _userMngr;
        }

        // GET: api/User
        public IEnumerable<UserDTO> Get()
        {
            return UserManager.Get();
        }

        // GET: api/User/5
        public UserDTO Get(int id)
        {
            return UserManager.Get(id);
        }

        // GET: api/User/email
        public UserDTO Get(string email)
        {
            return UserManager.Get(email);
        }

        // POST: api/User
        public void Post([FromBody] UserDTO entity)
        {
            UserManager.Add(entity);
        }

        // PUT: api/User/5
        public void Put([FromBody] UserDTO entity)
        {
            UserManager.Update(entity);
        }

        // DELETE: api/User/5
        public void Delete(int id)
        {
            UserManager.Delete(id);
        }
    }
}
