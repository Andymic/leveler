﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Leveler.Business.Managers.Interfaces;
using Common.Model.DealModel;
namespace Leveler.Controllers
{
    public class DealController : ApiController
    {
         private readonly IDealManager DealManager;

         public DealController(IDealManager _DealManager)
        {
            this.DealManager = _DealManager;
        }

         // GET: api/Deal
        public IEnumerable<DealDTO> Get()
        {
            return DealManager.Get();
        }

        // GET: api/Deal/5
        public DealDTO Get(int id)
        {
            return DealManager.Get(id);
        }

        // GET: api/Deal/company
        public DealDTO Get(string company)
        {
            return DealManager.Get(company);
        }

        // POST: api/Deal
        public void Post([FromBody] DealDTO entity)
        {
            DealManager.Add(entity);
        }

        // PUT: api/Deal/5
        public void Put([FromBody] DealDTO entity)
        {
            DealManager.Update(entity);
        }

        // DELETE: api/Deal/5
        public void Delete(int id)
        {
            DealManager.Delete(id);
        }
    }
}
