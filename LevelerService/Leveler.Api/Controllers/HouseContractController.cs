﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Leveler.Business.Managers;
using Common.Model.HouseModel;

namespace Leveler.Controllers
{
    public class HouseContractController : ApiController
    {
        private readonly HouseContractManager HouseContractManager;

        public HouseContractController(HouseContractManager _HouseContractManager)
        {
            this.HouseContractManager = _HouseContractManager;
        }

        // GET: api/HouseContract
        public IEnumerable<HouseContractDTO> Get()
        {
            return HouseContractManager.Get();
        }

        // GET: api/HouseContract/5
        public HouseContractDTO Get(int id)
        {
            return HouseContractManager.Get(id);
        }

        // GET: api/HouseContract/username
        public HouseContractDTO Get(string username)
        {
            return HouseContractManager.Get(username);
        }

        // POST: api/HouseContract
        public void Post([FromBody] HouseContractDTO entity)
        {
            HouseContractManager.Add(entity);
        }

        // PUT: api/HouseContract/5
        public void Put([FromBody] HouseContractDTO entity)
        {
            HouseContractManager.Update(entity);
        }

        // DELETE: api/HouseContract/5
        public void Delete(int id)
        {
            HouseContractManager.Delete(id);
        }
    }
}
