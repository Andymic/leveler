﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Leveler.Business.Managers;
using Common.Model.HouseModel;

namespace Leveler.Controllers
{
    public class HouseController : ApiController
    {
        private readonly IHouseManager HouseManager;

        public HouseController(IHouseManager _house)
        {
            this.HouseManager = _house;
        }

        // GET: api/House
        public IEnumerable<HouseDTO> Get()
        {
            return HouseManager.Get();
        }

        // GET: api/House/5
        public HouseDTO Get(int id)
        {
            return HouseManager.Get(id);
        }

        // GET: api/House/username
        public HouseDTO Get(string username)
        {
            return HouseManager.Get(username);
        }

        // POST: api/House
        public void Post([FromBody] HouseDTO entity)
        {
            HouseManager.Add(entity);
        }

        // PUT: api/House/5
        public void Put([FromBody] HouseDTO entity)
        {
            HouseManager.Update(entity);
        }

        // DELETE: api/House/5
        public void Delete(int id)
        {
            HouseManager.Delete(id);
        }
    }
}
