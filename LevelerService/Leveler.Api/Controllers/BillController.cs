﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Leveler.Business.Managers.Interfaces;
using Common.Model.BillModel;

namespace Leveler.Controllers
{
    public class BillController : ApiController
    {
        private readonly IBillManager BillManager;

        public BillController(IBillManager _BillManager)
        {
            this.BillManager = _BillManager;
        }

        // GET: api/Bill
        public IEnumerable<BillDTO> Get()
        {
            return BillManager.Get();
        }

        // GET: api/Bill/5
        public BillDTO Get(int id)
        {
            return BillManager.Get(id);
        }

        // GET: api/Bill/BillNickName
        public BillDTO Get(string nickname)
        {
            return BillManager.Get(nickname);
        }

        // POST: api/User
        public void Post([FromBody] BillDTO entity)
        {
            BillManager.Add(entity);
        }

        // PUT: api/User/5
        public void Put([FromBody] BillDTO entity)
        {
            BillManager.Update(entity);
        }

        // DELETE: api/User/5
        public void Delete(int id)
        {
            BillManager.Delete(id);
        }
    }
}
