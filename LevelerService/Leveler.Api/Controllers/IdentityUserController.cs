﻿using Common.Model;
using Common.Model.UserManagement;
using Leveler.Business.Managers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Leveler.Controllers
{
    public class IdentityUserController : ApiController
    {
        private readonly IIdentityUserManager IdentityUserManager;

        public IdentityUserController(IIdentityUserManager _IdentityUserMngr)
        {
            this.IdentityUserManager = _IdentityUserMngr;
        }

        // GET: api/IdentityUser
        public IEnumerable<IdentityUserDTO> Get()
        {
            return IdentityUserManager.Get();
        }

        // GET: api/IdentityUser/5
        public IdentityUserDTO Get(int id)
        {
            return IdentityUserManager.Get(id);
        }

        // GET: api/IdentityUser/email
        public IdentityUserDTO Get(string email)
        {
            return IdentityUserManager.Get(email);
        }

        [HttpPost]
        [Route("IdentityUser/ByExternalLogin")]
        public IdentityUserDTO ByExternalLogin([FromBody] UserExternalLoginDTO loginInfo)
        {
            return IdentityUserManager.Get(loginInfo);
        }

        [HttpPost]
        [Route("IdentityUser/AddExternalLogin")]
        public IdentityUserDTO AddExternalLogin([FromBody] UserExternalLoginDTO loginInfo)
        {
            return IdentityUserManager.AddExternalLogin(loginInfo);
        }

        // POST: api/IdentityUser
        public void Post([FromBody] IdentityUserDTO entity)
        {
            IdentityUserManager.Add(entity);
        }

        // PUT: api/IdentityUser/5
        public void Put([FromBody] IdentityUserDTO entity)
        {
            IdentityUserManager.Update(entity);
        }

        // DELETE: api/IdentityUser/5
        public void Delete(int id)
        {
            IdentityUserManager.Delete(id);
        }
    }
}
