﻿using Common.Model.HouseModel;
using Leveler.Business.Managers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Leveler.Controllers
{
    public class LandlordController : ApiController
    {
        private ILandlordManager LandlordManager;

        public LandlordController(ILandlordManager _landlordMgr)
        {
            this.LandlordManager = _landlordMgr;
        }

        // GET: api/Landlord
        public IEnumerable<LandlordDTO> Get()
        {
            return LandlordManager.Get();
        }

        // GET: api/Landlord/5
        public LandlordDTO Get(int id)
        {
            return LandlordManager.Get(id);
        }

        // POST: api/Landlord
        public void Post([FromBody]LandlordDTO value)
        {
            LandlordManager.Add(value);
        }

        // PUT: api/Landlord/5
        public void Put(int id, [FromBody]LandlordDTO value)
        {
            LandlordManager.Update(value);
        }

        // DELETE: api/Landlord/5
        public void Delete(int id)
        {
            LandlordManager.Delete(id);
        }
    }
}
