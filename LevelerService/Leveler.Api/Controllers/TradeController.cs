﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Leveler.Business.Managers.Interfaces;
using Common.Model.TradeModel;

namespace Leveler.Controllers
{
    public class TradeController : ApiController
    {
        private readonly ITradeManager TradeManager;

        public TradeController(ITradeManager _TradeManager)
        {
            this.TradeManager = _TradeManager;
        }

        // GET: api/Trade
        public IEnumerable<TradeDTO> Get()
        {
            return TradeManager.Get();
        }

        // GET: api/Bill/5
        public TradeDTO Get(int id)
        {
            return TradeManager.Get(id);
        }

        // GET: api/Bill/BillNickName
        public TradeDTO Get(string Item)
        {
            return TradeManager.Get(Item);
        }

    }
}
