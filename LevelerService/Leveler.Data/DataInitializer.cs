﻿using Common.Helpers;
using Leveler.Domain.POCO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Data
{
    class DataInitializer : DropCreateDatabaseAlways<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            try
            {
                List<User> seededUsers = new List<User>
            {
                new User{ UserType= UserType.Personal, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow, Email = "Email1@mail.com", LastLogin = DateTime.UtcNow, Password = "$2a$10$LewItDiH18n588DVObU50eFOvCTYghyLKsTsuVZ9gifvlP6H39PeW", UserName = "User1"},
                new User{ UserType= UserType.Student, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow, Email = "Email2@mail.com", LastLogin = DateTime.UtcNow, Password = "$2a$10$LewItDiH18n588DVObU50eFOvCTYghyLKsTsuVZ9gifvlP6H39PeW", UserName = "User2"},
                new User{ UserType= UserType.Landlord, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow, Email = "Email3@mail.com", LastLogin = DateTime.UtcNow, Password = "$2a$10$LewItDiH18n588DVObU50eFOvCTYghyLKsTsuVZ9gifvlP6H39PeW", UserName = "User3"},
                new User{ UserType= UserType.Business, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow, Email = "Email3@mail.com", LastLogin = DateTime.UtcNow, Password = "$2a$10$LewItDiH18n588DVObU50eFOvCTYghyLKsTsuVZ9gifvlP6H39PeW", UserName = "User3"}
            };
                seededUsers.ForEach(u => context.Set<User>().Add(u));
                context.SaveChanges();

                List<Landlord> seededLandlord = new List<Landlord>
            {
                new Landlord{ DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow, UserId = 1}
            };
                seededLandlord.ForEach(l => context.Set<Landlord>().Add(l));
                context.SaveChanges();

                List<Utilities> utils = new List<Utilities>
            {
                Utilities.Refrigerator,
                Utilities.Stove,
                Utilities.DishWasher
            };

                List<Location> seededLocations = new List<Location>
            {
                new Location{ DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow, City = "City", Latitude = "123456", Longitude = "654321", PostalCode ="33065", State = "FL", StreetAddress = "StreetOne" }
            };
                seededLocations.ForEach(l => context.Set<Location>().Add(l));
                context.SaveChanges();

                List<House> seededHouses = new List<House>
            {
                new House{ DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow, Details = "Details", LandlordId = 1, LocationId = 1, Price = 100.00, Utilities = utils },
            };
                seededHouses.ForEach(u => context.Set<House>().Add(u));
                context.SaveChanges();

                List<HouseContract> seededHouseContracts = new List<HouseContract>
            {
                new HouseContract{Status=ContractStatus.Active, DateCreated = DateTime.UtcNow, DateModified= DateTime.UtcNow, LandlordId = 1, Contract="Rent is 750"},
                new HouseContract{Status=ContractStatus.Expired, DateCreated = DateTime.UtcNow, DateModified= DateTime.UtcNow, LandlordId = 1, Contract="Rent is 750"},
                new HouseContract{Status=ContractStatus.Inactive, DateCreated = DateTime.UtcNow, DateModified= DateTime.UtcNow, LandlordId = 1, Contract="Rent is 750"},
                new HouseContract{Status=ContractStatus.Sealed, DateCreated = DateTime.UtcNow, DateModified= DateTime.UtcNow, LandlordId = 1, Contract="Rent is 750"}
            };
                seededHouseContracts.ForEach(u => context.Set<HouseContract>().Add(u));
                context.SaveChanges();

                List<Bill> seededBills = new List<Bill>
            {
                new Bill{HouseId=1, Amount="$145", Reminder="15th of every month" ,DueDate=DateTime.UtcNow, Status="Paid"},
                new Bill{HouseId=2, Amount="$145", Reminder="15th of every month" ,DueDate=DateTime.UtcNow, Status="Paid"},
                new Bill{HouseId=3, Amount="$145", Reminder="15th of every month" ,DueDate=DateTime.UtcNow, Status="Paid"}
            };

                List<Trade> seededTrades = new List<Trade>
            {
                new Trade{Active=true,Category="Bicycle",Item="Bike",Description="Moutain Bike"},
                new Trade{Active=true,Category="Automotive",Item="Car",Description="Tesla 2015"},
                new Trade{Active=true,Category="Automotive",Item="Boat",Description="A small one"}

            };

                List<Deal> seededDeals = new List<Deal>
            {
                new Deal{HouseId=1,Company="Walmart",Category="Kitchen",Description="Get it now or maybe not.", Item="Knife", Expire=DateTime.UtcNow, Status="Expired"},
                new Deal{HouseId=2,Company="Walgreens",Category="Health",Description="Get it now or maybe not.", Item="Dental Floss", Expire=DateTime.UtcNow, Status="Active"},
                new Deal{HouseId=3,Company="Trader Joes",Category="Fish",Description="Get it now or maybe not.", Item="Salmon", Expire=DateTime.UtcNow, Status="Expired"}
            };

                


                List<Landlord> seededLandlords = new List<Landlord>
                {
                    new Landlord{DateCreated=DateTime.Now, DateModified=DateTime.Now, UserId=1}
                };

                seededBills.ForEach(u => context.Set<Bill>().Add(u));
                seededTrades.ForEach(u => context.Set<Trade>().Add(u));
                seededDeals.ForEach(u => context.Set<Deal>().Add(u));
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {

            }
        }
    }
}
