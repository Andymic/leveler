﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Domain.POCO;

namespace Leveler.Data
{
    public class DataContext : DbContext, IDisposable
    {
        public new IDbSet<T> Set<T>() where T : class
        {
            return base.Set<T>();
        }

        public DataContext()
            : base("Name=Leveler")
        {
            Database.SetInitializer<DataContext>(new DataInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasMany(u => u.ExternalLogins).WithRequired(e => e.User).HasForeignKey(e => e.UserId).WillCascadeOnDelete(false);
            modelBuilder.Entity<User>().HasOptional(u => u.House).WithMany(h => h.Tenants).HasForeignKey(u => u.HouseId).WillCascadeOnDelete(false);
            
            
            modelBuilder.Entity<Landlord>().HasMany(l => l.Houses).WithRequired(h => h.Landlord).HasForeignKey(h => h.LandlordId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Landlord>().HasRequired(l => l.User);
            modelBuilder.Entity<Landlord>().HasMany(l => l.Contracts).WithRequired(hc => hc.Landlord).HasForeignKey(hc => hc.LandlordId);

            modelBuilder.Entity<HouseContract>().HasMany(hc => hc.Tenants).WithOptional(u => u.Contract).HasForeignKey(hc => hc.ContractId);
            
            modelBuilder.Entity<HouseContract>().HasOptional(hc => hc.House);
            

            modelBuilder.Entity<UserExternalLogin>();
            modelBuilder.Entity<House>();
            modelBuilder.Entity<Bill>();
            modelBuilder.Entity<Trade>();
            modelBuilder.Entity<Deal>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
