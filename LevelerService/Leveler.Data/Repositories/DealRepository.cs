﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Domain.POCO;
using Common.Data;
using Leveler.Data.Repositories.Interfaces;
using System.Data.Entity;
using System.Diagnostics.Contracts;

namespace Leveler.Data.Repositories
{
    public class DealRepository: BaseRepository<Deal, DataContext>, IDealRepository
    {
         private IDbSet<Deal> DealEntities
        {
            get { return this.Context.Set<Deal>(); }
        }

         public DealRepository(DataContext context)
            : base(context)
        {
            Contract.Requires(context != null);
        }
    }
}

