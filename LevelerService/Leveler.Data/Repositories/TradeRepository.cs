﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Domain.POCO;
using Common.Data;
using Leveler.Data.Repositories.Interfaces;
using System.Data.Entity;
using System.Diagnostics.Contracts;

namespace Leveler.Data.Repositories
{
    public class TradeRepository: BaseRepository<Trade,DataContext>, ITradeRepository
    {
        private IDbSet<Trade> UserEntities
        {
            get { return this.Context.Set<Trade>(); }
        }

        public TradeRepository(DataContext context)
            : base(context)
        {
            Contract.Requires(context != null);
        }
    }
}
