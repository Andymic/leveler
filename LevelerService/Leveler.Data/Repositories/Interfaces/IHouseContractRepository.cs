﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Data;
using Leveler.Domain.POCO;

namespace Leveler.Data.Repositories.Interfaces
{
    public interface IHouseContractRepository : IBaseRepository<HouseContract>
    {
    }
}
