﻿using Common.Data;
using Leveler.Domain.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Data.Repositories.Interfaces
{
    public interface ILandlordRepository:IBaseRepository<Landlord>
    {
    }
}
