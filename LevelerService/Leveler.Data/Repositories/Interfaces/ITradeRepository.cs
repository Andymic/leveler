﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Domain.POCO;
using Common.Data;

namespace Leveler.Data.Repositories.Interfaces
{
    public interface ITradeRepository: IBaseRepository<Trade>
    {
    }
}
