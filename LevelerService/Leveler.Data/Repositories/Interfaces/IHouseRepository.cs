﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Domain.POCO;

namespace Leveler.Data.Repositories.Interfaces
{
    public interface IHouseRepository : IBaseRepository<House>
    {
    }
}
