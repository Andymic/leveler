﻿using Common.Data;
using Leveler.Domain.POCO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Data.Repositories
{
    public class UserExternalLoginRepository : BaseRepository<UserExternalLogin, DataContext>, IUserExternalLoginRepository
    {
        private IDbSet<UserExternalLogin> UserEntities
        {
            get { return this.Context.Set<UserExternalLogin>(); }
        }

        public UserExternalLoginRepository(DataContext context)
            : base(context)
        {
            Contract.Requires(context != null);
        }
    }
}
