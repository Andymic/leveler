﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Domain.POCO;

namespace Leveler.Data.Repositories
{
    public class UserRepository:BaseRepository<User,DataContext>,IUserRepository
    {
        private IDbSet<User> UserEntities
        {
            get { return this.Context.Set<User>(); }
        }

        public UserRepository(DataContext context)
            : base(context)
        {
            Contract.Requires(context != null);
        }

        public void UserById(int id)
        {

        }
    }
}
