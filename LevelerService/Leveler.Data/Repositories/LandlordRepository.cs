﻿using Common.Data;
using Leveler.Data.Repositories.Interfaces;
using Leveler.Domain.POCO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Data.Repositories
{
    public class LandlordRepository : BaseRepository<Landlord, DataContext>, ILandlordRepository
    {
        private IDbSet<Landlord> LandlordEntities
        {
            get { return this.Context.Set<Landlord>(); }
        }

        public LandlordRepository(DataContext context)
            : base(context)
        {
            Contract.Requires(context != null);
        }
    }
}
