﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Domain.POCO;
using Common.Data;
using Leveler.Data.Repositories.Interfaces;
using System.Data.Entity;
using System.Diagnostics.Contracts;

namespace Leveler.Data.Repositories
{

    public class HouseRepository : BaseRepository<House, DataContext>, IHouseRepository
    {
        private IDbSet<House> UserEntities
        {
            get { return this.Context.Set<House>(); }
        }

        public HouseRepository(DataContext context)
            : base(context)
        {
            Contract.Requires(context != null);
        }
    }
}
