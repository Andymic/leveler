﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Leveler.Domain.POCO;
using Common.Data;
using Leveler.Data.Repositories.Interfaces;
using System.Data.Entity;
using System.Diagnostics.Contracts;

namespace Leveler.Data.Repositories
{
    public class BillRepository : BaseRepository<Bill, DataContext>, IBillRepository
    {
         private IDbSet<Bill> BillEntities
        {
            get { return this.Context.Set<Bill>(); }
        }

         public BillRepository(DataContext context)
            : base(context)
        {
            Contract.Requires(context != null);
        }
    }
}
