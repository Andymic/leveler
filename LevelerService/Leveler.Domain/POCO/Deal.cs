﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Leveler.Domain.POCO
{
    [Table("Deals")]
    public class Deal
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int HouseId;

        public string Company { get; set; }

        public string Category { get; set; }

        public string Description { get; set; }

        public string Item { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime Expire { get; set; }

        public string Status { get; set; }
    }
}
