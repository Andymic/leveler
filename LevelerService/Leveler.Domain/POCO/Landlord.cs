﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Domain.POCO
{
    [Table("Landlords")]
    public class Landlord
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime DateCreated { get; set; }

        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime DateModified { get; set; }

        [Required]
        public int UserId { get; set; }

        //The User account which is a landlord
        public virtual User User { get; set; }
        //The houses which the landlord owns
        public virtual ICollection<House> Houses { get; set; }

        public virtual ICollection<HouseContract> Contracts { get; set; }
    }
}
