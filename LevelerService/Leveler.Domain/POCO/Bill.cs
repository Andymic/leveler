﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Leveler.Domain.POCO
{
    [Table("Bills")]
    public class Bill
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
 
        public int HouseId;

        public int[] UserId;

        public string Amount { get; set; }

        public string Nickname { get; set; }

        public string Reminder { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime DueDate { get; set; }

        public string Status { get; set; }
    }
}
