﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Helpers;
using Common.Model.HouseModel;

namespace Leveler.Domain.POCO
{
    [Table("Houses")]
    public class House
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime DateCreated { get; set; }

        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime DateModified { get; set; }

        //[Required]
        //public Location Location { get; set; }

        public double Price { get; set; }


        public string Details { get; set; }

        public List<Utilities> Utilities { get; set; }

        [Required]
        public int LandlordId { get; set; }

        [Required]
        public int LocationId { get; set; }

        public virtual Location Location { get; set; }
        public virtual Landlord Landlord { get; set; }
        public virtual ICollection<User> Tenants { get; set; }
    }
}
