﻿using Common.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Domain.POCO
{
    [Table("LevelerUser")]
    public class User
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime DateCreated { get; set; }

        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime DateModified { get; set; }

        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime LastLogin { get; set; }

        [Required]
        public UserType UserType { get; set; }

        //[Required] Looking to not use Username, and only use Email for login
        public string UserName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public int? HouseId { get; set; }

        public int? LandlordId { get; set; }

        public int? ContractId { get; set; }

        public virtual ICollection<UserExternalLogin> ExternalLogins { get; set; }
        public virtual House House { get; set; }
        public virtual HouseContract Contract { get; set; }
    }
}
