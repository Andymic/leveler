﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Helpers;

namespace Leveler.Domain.POCO
{
    [Table("Contract")]
    public class HouseContract
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime DateCreated { get; set; }

        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime DateModified { get; set; }
        
        public ContractStatus Status { get; set; }

        public int LandlordId { get; set; }
        public string Contract { get; set; }
        public virtual House House { get; set; }
        public virtual Landlord Landlord { get; set; }
        public virtual ICollection<User> Tenants { get; set; }
    }
}
