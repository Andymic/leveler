﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Domain.POCO
{
    [Table("UserExternalLogins")]
    public class UserExternalLogin
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string LoginProvider { get; set; }

        [Required]
        public string ProviderKey { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}
