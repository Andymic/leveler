//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Leveler.Domain.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Bill
    {
        public int BillId { get; set; }
        public Nullable<long> Recipient { get; set; }
        public Nullable<long> Sender { get; set; }
        public string Comment { get; set; }
        public Nullable<long> Amount { get; set; }
        public string Currency { get; set; }
    }
}
