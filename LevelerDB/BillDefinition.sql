﻿CREATE TABLE [dbo].[BillDefinition]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Type] INT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [Frequency] NVARCHAR(50) NULL, 
    [Reminder] DATETIME2 NULL
)
