﻿CREATE TABLE [dbo].[UserSetting]
(
	[Id] INT NOT NULL IDENTITY , 
    [UserId] INT NOT NULL, 
    [IsOwner] BIT NULL, 
    [IsAdmin] BIT NULL, 
    [IsActive] BIT NULL, 
    PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_UserSetting] FOREIGN KEY ([UserId]) REFERENCES [LevelerUser]([UserId]) 
)
