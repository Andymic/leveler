﻿CREATE TABLE [dbo].[Location]
(
	[LocationId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [State] NVARCHAR(50) NULL, 
    [City] NCHAR(10) NULL, 
    [Coordinates] NCHAR(10) NULL 
)
