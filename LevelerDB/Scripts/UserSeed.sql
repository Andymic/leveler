﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

USE Leveler
GO
DECLARE @COUNTER INT

SET @COUNTER=0
 
WHILE @COUNTER < 100
BEGIN
	SET @COUNTER += 1
   INSERT INTO  LevelerUser (UserName, Email, Password, LocationID,Role,secondaryEmail,AccountType)
      Values('Zombie'+CAST(@COUNTER AS varchar), 'Email@mail.com','password1',null,1,'',1)
END
PRINT 'Seeded Users complete...'