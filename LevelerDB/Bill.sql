﻿CREATE TABLE [dbo].[Bill]
(
	[BillId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Recipient] BIGINT NULL, 
    [Sender] BIGINT NULL, 
    [Comment] NVARCHAR(MAX) NULL, 
    [Amount] BIGINT NULL, 
    [Currency] NCHAR(10) NULL
)
