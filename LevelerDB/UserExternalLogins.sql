﻿CREATE TABLE [dbo].[UserExternalLogins]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [LoginProvider] NVARCHAR(MAX) NOT NULL, 
    [ProviderKey] NVARCHAR(MAX) NOT NULL, 
    [UserId] INT NOT NULL, 
    CONSTRAINT [FK_UserExternalLogins] FOREIGN KEY ([UserId]) REFERENCES [LevelerUser]([UserId]) 
)
