﻿CREATE TABLE [dbo].[Address]
(
	[AddressId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Suburb] NVARCHAR(200) NOT NULL, 
    [City] NVARCHAR(100) NOT NULL, 
    [State] NVARCHAR(20) NOT NULL, 
    [ZipCode] INT NOT NULL, 
    [Country] NVARCHAR(50) NOT NULL, 
    [Longitude] NVARCHAR(1000) NULL, 
    [Lattitude] NVARCHAR(1000) NULL, 
    [UserId] INT NOT NULL, 
    CONSTRAINT [FK_AddressId] FOREIGN KEY ([UserId]) REFERENCES [LevelerUser]([UserId])
)
