﻿CREATE TABLE [dbo].[LevelerUser]
(
	[UserId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [UserName] NVARCHAR(50) NULL, 
    [Email] NVARCHAR(50) NOT NULL, 
    [Password] NVARCHAR(MAX) NOT NULL, 
    [LocationID] BIGINT NULL, 
    [Role] INT NULL, 
    [SecondaryEmail] NVARCHAR(50) NULL, 
    [AccountType] INT NULL 
)
