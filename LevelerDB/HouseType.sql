﻿CREATE TABLE [dbo].[HomeType]
(
	[HouseTypeId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Description] NCHAR(10) NULL
)
