﻿CREATE TABLE [dbo].[AccountType]
(
	[AccountTypeId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Type] NVARCHAR(50) NOT NULL
)
