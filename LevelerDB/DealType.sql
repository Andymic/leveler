﻿CREATE TABLE [dbo].[DealType]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Category] NVARCHAR(50) NULL, 
    [Conditions] NCHAR(10) NULL, 
    [DealId] INT NULL, 
    CONSTRAINT [FK_DealType] FOREIGN KEY ([DealId]) REFERENCES [Deal]([DealId])
)
