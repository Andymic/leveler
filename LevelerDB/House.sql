﻿CREATE TABLE [dbo].[Home]
(
	[HouseId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [HouseType] INT NOT NULL, 
    [AddressId] INT NOT NULL, 
    [Rooms] INT NOT NULL, 
    [UserId] INT NOT NULL, 
    CONSTRAINT [FK_Home] FOREIGN KEY ([UserId]) REFERENCES [LevelerUser]([UserId]), 
    CONSTRAINT [FK_Home_ToAddress] FOREIGN KEY ([AddressId]) REFERENCES [Address]([AddressId])
)
