﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Web.Config
{
    public class ServiceHostSettingsCollection : ConfigurationElementCollection
    {
        public ServiceHostSettings this[int index]
        {
            get { return (ServiceHostSettings)BaseGet(index); }
        }

        public new ServiceHostSettings this[string key]
        {
            get { return (ServiceHostSettings)BaseGet(key); }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ServiceHostSettings();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ServiceHostSettings)element).Name;
        }
    }
}
