﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Web.Config
{
    public class ServiceHostsSection : ConfigurationSection
    {
        [ConfigurationProperty("services")]
        public ServiceHostSettingsCollection Services
        {
            get { return (ServiceHostSettingsCollection)base["services"]; }
        }

        public static ServiceHostsSection GetSection()
        {
            return ConfigurationManager.GetSection("Leveler.Mvc/Leveler.Web/serviceHosts") as ServiceHostsSection;
        }

        public static ServiceHostSettings ServiceHostSettings(string key)
        {
            return GetSection().Services[key] as ServiceHostSettings;
        }
    }
}
