﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Leveler.Web.Config
{
    public sealed class ServiceHostSettings : ConfigurationElement
    {
        public ServiceHostSettings()
        {
        }

        public ServiceHostSettings(string name, string host)
        {
            this.Name = name;
            this.Host = host;
        }

        [ConfigurationProperty("host", IsRequired = true)]
        public string Host
        {
            get
            {
                string host = (string)this["host"];
                if (!host.EndsWith("/"))
                {
                    host = host + "/";
                }

                return host;
            }
            set
            {
                this["host"] = value;
            }
        }

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }
    }
}
