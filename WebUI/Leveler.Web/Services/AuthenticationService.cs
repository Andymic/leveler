﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model;
using System.Security.Claims;
using Microsoft.Owin.Security;
using Common.Helpers;
using System.Web;
using System.Threading;
using System.Security.Principal;
using System.IdentityModel.Tokens;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Services;
using Microsoft.AspNet.Identity.EntityFramework;
using Common.Model.UserManagement;

namespace Leveler.Web.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUserService UserService;

        public AuthenticationService(IUserService _userService)
        {
            this.UserService = _userService;
        }

        #region UserStore Methods
        public Task CreateAsync(IdentityUserDTO user)
        {
            return this.UserService.Add(user);
        }

        public Task DeleteAsync(IdentityUserDTO user)
        {
            return this.UserService.Delete(user.Id);
        }

        public Task<IdentityUserDTO> FindByIdAsync(int userId)
        {
            return this.UserService.GetIdentityUser(userId);
        }

        public Task<IdentityUserDTO> FindByNameAsync(string email)
        {
            //Lets use email instead, as we will not be using a username
            return this.UserService.GetIdentityUser(email);
        }

        public Task UpdateAsync(IdentityUserDTO user)
        {
            return this.UserService.Update(user);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region UserPasswordStore Methods
        public Task<string> GetPasswordHashAsync(IdentityUserDTO user)
        {
            return Task.FromResult(user.Password);
        }

        public Task<bool> HasPasswordAsync(IdentityUserDTO user)
        {
            return Task.FromResult(true); ;
        }

        public Task SetPasswordHashAsync(IdentityUserDTO user, string passwordHash)
        {
                user.Password = passwordHash;
                return UserService.Update(user);
        }
        #endregion

        #region UserLoginStore Methods
        public Task AddLoginAsync(IdentityUserDTO user, UserLoginInfo login)
        {
            UserExternalLoginDTO loginInfo = new UserExternalLoginDTO(login.LoginProvider, login.ProviderKey, user.Id);

            return this.UserService.AddExternalLogin(loginInfo);
        }

        public Task<IdentityUserDTO> FindAsync(UserLoginInfo login)
        {
            UserExternalLoginDTO loginInfo = new UserExternalLoginDTO(login.LoginProvider, login.ProviderKey, null);
            var user = this.UserService.GetIdentityUser(loginInfo);
            return user;
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(IdentityUserDTO user)
        {
            throw new NotImplementedException();
        }

        public Task RemoveLoginAsync(IdentityUserDTO user, UserLoginInfo login)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region UserClaimsStore Methods
        public Task AddClaimAsync(IdentityUserDTO user, Claim claim)
        {
            user.Claims.Add(new IdentityUserClaim());
            return Task.FromResult(true);
        }

        public Task<IList<Claim>> GetClaimsAsync(IdentityUserDTO user)
        {
            IList<Claim> tempClaims = new List<Claim> 
            { 
                new Claim(ClaimTypes.Email, user.Email, DefaultAuthenticationTypes.ExternalBearer),
                new Claim("Id", user.Id.ToString(), DefaultAuthenticationTypes.ExternalBearer),
                new Claim("UserType", user.UserType.ToString(), DefaultAuthenticationTypes.ExternalBearer),
                new Claim("LandlordId", user.LandlordId.ToString(), DefaultAuthenticationTypes.ExternalBearer)
            };

            return Task.FromResult(tempClaims); ;
        }

        public Task RemoveClaimAsync(IdentityUserDTO user, Claim claim)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region UserRoleStore Methods
        public Task AddToRoleAsync(IdentityUserDTO user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task<IList<string>> GetRolesAsync(IdentityUserDTO user)
        {
            IList<string> tempRoles = new List<string>{"User"};
            return Task.FromResult(tempRoles);
        }

        public Task<bool> IsInRoleAsync(IdentityUserDTO user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task RemoveFromRoleAsync(IdentityUserDTO user, string roleName)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region UserLockoutStore Methods
        public Task<int> GetAccessFailedCountAsync(IdentityUserDTO user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetLockoutEnabledAsync(IdentityUserDTO user)
        {
            return new Task<bool>(() => false);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(IdentityUserDTO user)
        {
            throw new NotImplementedException();
        }

        public Task<int> IncrementAccessFailedCountAsync(IdentityUserDTO user)
        {
            throw new NotImplementedException();
        }

        public Task ResetAccessFailedCountAsync(IdentityUserDTO user)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEnabledAsync(IdentityUserDTO user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEndDateAsync(IdentityUserDTO user, DateTimeOffset lockoutEnd)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
