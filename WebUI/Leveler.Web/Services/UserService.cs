﻿using Common.Helpers;
using Common.Model;
using Common.Model.UserManagement;
using Leveler.Web.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Web.Services
{
    public class UserService:IUserService
    {
        private static readonly Uri HostUri = new Uri(ServiceHostsSection.ServiceHostSettings("LevelerService").Host);

        #region Users
        public async Task<List<UserDTO>> Get()
        {
            return await RestServiceHelper.ServiceGetAsync<List<UserDTO>>(HostUri, "User", AcceptMediaTypes.ApplicationXml);
        }

        public async Task<UserDTO> Add(UserDTO user)
        {
            return await RestServiceHelper.ServicePostAsync<UserDTO,UserDTO>(user, HostUri, "User",  AcceptMediaTypes.ApplicationXml);
        }

        public async Task Delete(int id)
        {
            await RestServiceHelper.ServiceDeleteAsync(HostUri, id, "User", AcceptMediaTypes.ApplicationXml);
        }

        public async Task<UserDTO> Get(int id)
        {
            return await RestServiceHelper.ServiceGetAsync<UserDTO>(HostUri, string.Format("User/?id={0}", id), AcceptMediaTypes.ApplicationXml);
        }

        public async Task<UserDTO> Get(string email)
        {
            return await RestServiceHelper.ServiceGetAsync<UserDTO>(HostUri, string.Format("User/?email={0}", email), AcceptMediaTypes.ApplicationXml);
        }

        public async Task Update(UserDTO user)
        {
            await RestServiceHelper.ServicePutAsync<UserDTO>(user, HostUri, "User", AcceptMediaTypes.ApplicationXml);
        }
        #endregion

        #region Identity Users
        public async Task<List<IdentityUserDTO>> GetIdentityUser()
        {
            return await RestServiceHelper.ServiceGetAsync<List<IdentityUserDTO>>(HostUri, "IdentityUser", AcceptMediaTypes.ApplicationXml);
        }

        public async Task<IdentityUserDTO> Add(IdentityUserDTO IdentityUser)
        {
            return await RestServiceHelper.ServicePostAsync<IdentityUserDTO, IdentityUserDTO>(IdentityUser, HostUri, "IdentityUser", AcceptMediaTypes.ApplicationXml);
        }

        public async Task<IdentityUserDTO> GetIdentityUser(int id)
        {
            return await RestServiceHelper.ServiceGetAsync<IdentityUserDTO>(HostUri, string.Format("IdentityUser/?id={0}", id), AcceptMediaTypes.ApplicationXml);
        }

        public async Task<IdentityUserDTO> GetIdentityUser(string email)
        {
            return await RestServiceHelper.ServiceGetAsync<IdentityUserDTO>(HostUri, string.Format("IdentityUser/?email={0}", email), AcceptMediaTypes.ApplicationXml);
        }

        public async Task Update(IdentityUserDTO IdentityUser)
        {
            await RestServiceHelper.ServicePutAsync<IdentityUserDTO>(IdentityUser, HostUri, "IdentityUser", AcceptMediaTypes.ApplicationXml);
        }

        public async Task<IdentityUserDTO> GetIdentityUser(UserExternalLoginDTO loginInfo)
        {
            return await RestServiceHelper.ServicePostAsync<IdentityUserDTO, UserExternalLoginDTO>(loginInfo, HostUri, "IdentityUser/ByExternalLogin", AcceptMediaTypes.ApplicationXml);
        }

        public async Task<IdentityUserDTO> AddExternalLogin(UserExternalLoginDTO loginInfo)
        {
            return await RestServiceHelper.ServicePostAsync<IdentityUserDTO, UserExternalLoginDTO>(loginInfo, HostUri, "IdentityUser/AddExternalLogin", AcceptMediaTypes.ApplicationXml);
        }
        #endregion
    }
}
