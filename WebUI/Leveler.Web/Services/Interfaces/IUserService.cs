﻿using Common.Model;
using Common.Model.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Web.Services
{
    public interface IUserService
    {
        Task<List<UserDTO>> Get();
        Task<UserDTO> Add(UserDTO user);
        Task Delete(int id);
        Task<UserDTO> Get(int id);
        Task<UserDTO> Get(string username);
        Task Update(UserDTO user);

        Task<List<IdentityUserDTO>> GetIdentityUser();
        Task<IdentityUserDTO> Add(IdentityUserDTO IdentityUser);
        Task<IdentityUserDTO> GetIdentityUser(int id);
        Task<IdentityUserDTO> GetIdentityUser(string IdentityUsername);
        Task<IdentityUserDTO> GetIdentityUser(UserExternalLoginDTO loginInfo);
        Task<IdentityUserDTO> AddExternalLogin(UserExternalLoginDTO loginInfo);
        Task Update(IdentityUserDTO IdentityUser);
    }
}
