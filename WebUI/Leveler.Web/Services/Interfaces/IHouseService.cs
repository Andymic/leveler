﻿using Common.Model.HouseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leveler.Web.Services.Interfaces
{
    public interface IHouseService
    {
        Task<List<HouseDTO>> Get();
        Task<HouseDTO> Add(HouseDTO house);
        Task Delete(int id);
        Task<HouseDTO> Get(int id);
        Task Update(HouseDTO house);
    }
}
