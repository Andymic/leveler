﻿using Common.Model;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Leveler.Web.Services
{
    public interface IAuthenticationService : IUserStore<IdentityUserDTO, int>, IUserPasswordStore<IdentityUserDTO, int>, IUserLoginStore<IdentityUserDTO, int>, IUserClaimStore<IdentityUserDTO, int>, IUserRoleStore<IdentityUserDTO, int>, IUserLockoutStore<IdentityUserDTO, int>
    {
    }
}
