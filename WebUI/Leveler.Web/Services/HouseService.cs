﻿using Leveler.Web.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Model.HouseModel;
using Common.Helpers;
using Leveler.Web.Config;

namespace Leveler.Web.Services
{
    public class HouseService: IHouseService
    {
        private static readonly Uri HostUri = new Uri(ServiceHostsSection.ServiceHostSettings("LevelerService").Host);

        public async Task<List<HouseDTO>> Get()
        {
            return await RestServiceHelper.ServiceGetAsync<List<HouseDTO>>(HostUri, "House", AcceptMediaTypes.ApplicationXml);
        }

        public async Task<HouseDTO> Add(HouseDTO house)
        {
            return await RestServiceHelper.ServicePostAsync<HouseDTO, HouseDTO>(house, HostUri, "House", AcceptMediaTypes.ApplicationXml);
        }

        public async Task Delete(int id)
        {
            await RestServiceHelper.ServiceDeleteAsync(HostUri, id, "House", AcceptMediaTypes.ApplicationXml);
        }

        public async Task<HouseDTO> Get(int id)
        {
            return await RestServiceHelper.ServiceGetAsync<HouseDTO>(HostUri, string.Format("House/?id={0}", id), AcceptMediaTypes.ApplicationXml);
        }

        public async Task Update(HouseDTO house)
        {
            await RestServiceHelper.ServicePutAsync<HouseDTO>(house, HostUri, "House", AcceptMediaTypes.ApplicationXml);
        }
    }
}
