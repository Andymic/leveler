﻿using Common.Helpers;
using Common.Model.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leveler.Mvc.Controllers
{
    public class ProfileController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.UserType = CurrentUser.UserType.ToString();
            //Should never get this far
            return View();
        }

        // GET: Profile
        public ActionResult Personal()
        {
            return View();
        }

        public ActionResult Student()
        {
            return View();
        }
        public ActionResult Landlord()
        {
            return View();
        }
        public ActionResult Business()
        {
            return View();
        }

        public ActionResult Bill()
        {
            return View();
        }

        public ActionResult Deal()
        {
            return View();
        }
        
        public ActionResult Home()
        {
            return View();
        }
        // GET: Profile/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Profile/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Profile/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Profile/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Profile/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Profile/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Profile/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
