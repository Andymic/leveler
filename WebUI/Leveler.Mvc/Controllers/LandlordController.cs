﻿using Common.Model.HouseModel;
using Leveler.Web.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leveler.Mvc.Controllers
{
    public class LandlordController : Controller
    {
        private readonly IHouseService HouseService;

        public LandlordController(IHouseService _houseService)
        {
            this.HouseService = _houseService;
        }

        // GET: Landlord
        public ActionResult Index()
        {
            var homes = HouseService.Get();
            return View();
        }

        public ActionResult Contract(int? id)
        {
            return View();
        }
        // GET: Landlord/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Landlord/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Landlord/Create
        [HttpPost]
        public ActionResult Create(HouseDTO house)
        {
            try
            {
                HouseService.Add(house);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Landlord/Edit/5
        public ActionResult Edit(int id)
        {
            var house = HouseService.Get(id);
            return View();
        }

        // POST: Landlord/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Landlord/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Landlord/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
