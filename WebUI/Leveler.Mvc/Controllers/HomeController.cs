﻿using Common.Helpers;
using Leveler.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Leveler.Mvc.Controllers
{
    [RequireHttps]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            if(CurrentUser.IsAuthenticated)
            {
                if (CurrentUser.UserType != UserType.None)
                {
                    return RedirectToAction(CurrentUser.UserType.ToString(), "Profile");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid user type");
                }
            }

            return View();
        }

        public ActionResult Landing()
        {
            return View();
        }
        public PartialViewResult Bills()
        {
            PartialViewResult viewResult = new PartialViewResult();
            viewResult.ViewName = "This is the bills partial view";
            
            return viewResult;
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public async Task<ActionResult> Users()
        {
            UserService service = new UserService();

            var users = await service.Get();

            return View(users);
        }
    }
}