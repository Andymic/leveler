﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leveler.Mvc.Controllers
{
    public class AngularRoutesController : BaseController
    {
        // GET: AngularRoutes
        public ActionResult Index()
        {
            ViewBag.UserType = CurrentUser.UserType.ToString();
            return View();
        }

        // GET: AngularRoutes/Details/5
        public ActionResult Neighbor()
        {
            return View();
        }

        // GET: AngularRoutes/Trade
        public ActionResult Trade()
        {
            return View();
        }

        public ActionResult Events()
        {
            return View();
        }
        // POST: AngularRoutes/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AngularRoutes/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AngularRoutes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AngularRoutes/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AngularRoutes/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
