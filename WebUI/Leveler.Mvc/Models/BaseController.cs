﻿using Common.Model.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Leveler.Mvc
{
    public class BaseController:Controller
    {
        public CustomIdentity CurrentUser
        {
            get 
            {
                try
                {
                    if (base.User.Identity.IsAuthenticated==false)
                    {
                        return new CustomIdentity(new ClaimsIdentity());
                    }
                    else
                    {
                        return (CustomIdentity)base.User.Identity;
                    }
                }
                catch(Exception ex)
                {
                    throw new HttpUnhandledException(ex.StackTrace.ToString());
                }
            }
        }
    }
}