﻿using System.Web;
using System.Web.Optimization;

namespace Leveler.Mvc
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/LevelerAngularMVC")
                .IncludeDirectory("~/Scripts/Controllers", "*.js")
                .Include("~/Scripts/LevelerAngularApp/LevelerAngularApp.js")
                .Include("~/Scripts/angular-ui-router.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/ninestars").Include(
                        "~/Scripts/jquery.easing.min.js",
                        "~/Scripts/classie.js",
                        "~/Scripts/gnmenu.js",
                        "~/Scripts/jquery.scrollTo.js",
                        "~/Scripts/nivo-lightbox.min.js",
                        "~/Scripts/stellar.js",
                        "~/Scripts/custom.js",
                        "~/Scripts/xeditable.js"
                ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/style.css",
                        "~/Content/nivo-lightbox.css",
                        "~/Content/default.css",
                        "~/Content/animate.css",
                        "~/Content/color_default.css",
                        "~/font_awesome/font-awesome.min.css",
                        "~/Content/xeditable.css"
                      ));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }
}
