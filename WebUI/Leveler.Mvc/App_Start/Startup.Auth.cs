﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using Leveler.Mvc.Models;
using Common.Model;
using Leveler.Web.Services;
using System.IdentityModel.Protocols.WSTrust;

namespace Leveler.Mvc
{
    public partial class Startup
    {

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            //app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            //app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);
            
            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ExternalBearer,
                LoginPath = new PathString("/Account/Login")
                //Provider = new CookieAuthenticationProvider
                //{
                //    // Enables the application to validate the security stamp when the user logs in.
                //    // This is a security feature which is used when you change a password or add an external login to your account.  
                //    OnValidateIdentity = SecurityStampValidator
                    
                //    .OnValidateIdentity<ApplicationUserManager, IdentityUserDTO, int>(
                //        validateInterval: TimeSpan.FromMinutes(30),
                //        regenerateIdentityCallback: (manager, user) => user.GenerateUserIdentityAsync(manager),
                //        getUserIdCallback: (claim) => int.Parse(claim.GetUserId()))
                //}
            });            
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            //app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            //app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            app.UseTwitterAuthentication(
               consumerKey: "PZOwyJDIVFI4eHdzghIz8cVWI",
               consumerSecret: "ApDnH8t0u8U0j8pskV1qpTnGiVqIoR0yAEqxIVlT6N8le11A60");

            app.UseFacebookAuthentication(
               appId: "724537400994087",
               appSecret: "672ca9851e95386a42a0ed1e6c55e233");

            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "1053029438674-d06f3j0j8g2nhu1l0sfc05v5kopnpe48.apps.googleusercontent.com",
                ClientSecret = "O2jytq64Od5StVMWX0pxpoWh"
            });
        }
    }
}