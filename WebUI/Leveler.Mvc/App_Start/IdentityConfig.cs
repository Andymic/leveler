﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Leveler.Mvc.Models;
using Common.Model;
using Leveler.Web.Services;
using Common.Model.UserManagement;

namespace Leveler.Mvc
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<IdentityUserDTO, int>
    {
        private readonly IAuthenticationService AuthService;

        public ApplicationUserManager(IAuthenticationService _authService)
            : base(_authService)
        {
            this.AuthService = _authService;
        }

        new public async Task<CustomIdentity> CreateIdentityAsync(IdentityUserDTO user, string authenticationType)
        {
            var baseIdentity = await base.CreateIdentityAsync(user, authenticationType);

            return new CustomIdentity(baseIdentity);
        }

        public static ApplicationUserManager Create(IAuthenticationService _authService) 
        {
            var manager = new ApplicationUserManager(_authService);
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<IdentityUserDTO, int>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            //manager.UserLockoutEnabledByDefault = true;
            //manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            //manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            //manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser>
            //{
            //    MessageFormat = "Your security code is {0}"
            //});
            //manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
            //{
            //    Subject = "Security Code",
            //    BodyFormat = "Your security code is {0}"
            //});
            //manager.EmailService = new EmailService();
            //manager.SmsService = new SmsService();
            //var dataProtectionProvider = options.DataProtectionProvider;
            //if (dataProtectionProvider != null)
            //{
            //    manager.UserTokenProvider = 
            //        new DataProtectorTokenProvider<UserDTO>(dataProtectionProvider.Create("ASP.NET Identity"));
            //}
            return manager;
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<IdentityUserDTO, int>
    {
        //private readonly IAuthenticationService AuthService;

        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager _authService)
            : base(userManager, _authService)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(IdentityUserDTO user)
        {
            return base.CreateUserIdentityAsync(user);
        }

        //public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        //{
        //    return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        //}
    }
}
