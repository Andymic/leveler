﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Leveler.Mvc.Startup))]
namespace Leveler.Mvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
