﻿var LevelerAngularMVC = angular.module('LevelerAngularMVC', ['ngRoute', 'ngResource', 'xeditable', 'ui.router']);

LevelerAngularMVC.controller(['ProfileController', 'TradeController', 'AccountController', 'LandlordController', 'LoginController', 'RegisterController']
    , ProfileController, TradeController, AccountController, LandlordController, LoginController, RegisterController);

ProfilePageController.$inject = ['$scope'];

LevelerAngularMVC.run(function (editableOptions) {
    editableOptions.theme = 'bs3';
});

var configFunction = function ($routeProvider, $stateProvider) {

    $stateProvider
        .state('login',
        {
            url: '/login',
            templateUrl: 'Account/Login',
            controller: 'LoginController'
        })
        .state('register',
        {
            url: '/register',
            templateUrl: 'Account/Register',
            controller: 'RegisterController'
        });

    $routeProvider.
        when('/Profile', {
            templateUrl: 'AngularRoutes/Index',
            controller: 'ProfileController'
        })
        .when('/Neighbor', {
            templateUrl: 'AngularRoutes/Neighbor',
            controller: 'ProfileController'
        })
        .when('/Trade', {
            templateUrl: 'AngularRoutes/Trade',
            controller: 'ProfileController'
        })
        .when('/Events', {
            templateUrl: 'AngularRoutes/Events',
            controller: 'ProfileController'
        })
        .when('/Contract', {
            templateUrl: 'Landlord/Contract',
            controller: 'LandlordController'
        })
        .otherwise({
            redirectTo: '/Profile'
        });
}
configFunction.$inject = ['$routeProvider', '$stateProvider'];

LevelerAngularMVC.config(configFunction);