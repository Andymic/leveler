﻿var LevelerAngularMVC = angular.module('LevelerAngularMVC', ['ngRoute', 'ngResource']);

var ProfilePageController = function ($scope) {
    $scope.models = {
       'helloAngular': 'I work Andy!'
    };
}

var BillController = function ($http,$scope) {
    $scope.GetBills = function () {
        var response = $http.get('/Leveler.Api/Bill');
        response.success(function (data, status, headers, config) {

            $scope.bills = data;

        });

        response.error(function (data, status, headers, config) {
            alert("Error.");
        });
    };
};

var TradeController = function ($http, $scope) {
    $scope.GetTrades = function () {
        var response = $http.get('/Leveler.Api/Trade');
        response.success(function (data, status, headers, config) {

            $scope.trades = data;

        });

        response.error(function (data, status, headers, config) {
            alert("Error.");
        });
    };
};

ProfilePageController.$inject = ['$scope'];

LevelerAngularMVC.controller(['ProfilePageController', 'BillController', 'TradeController'], ProfilePageController, BillController, TradeController);

var configFunction = function ($routeProvider) {
    $routeProvider.
        when('/Profile', {
            templateUrl: 'AngularRoutes/Index',
            controller: 'ProfilePageController'
        })
        .when('/Neighbor', {
            templateUrl: 'AngularRoutes/Neighbor',
            controller: 'ProfilePageController'
        })
        .when('/Trade', {
            templateUrl: 'AngularRoutes/Trade',
            controller: 'ProfilePageController'
        })
        .when('/Events', {
            templateUrl: 'AngularRoutes/Events',
            controller: 'ProfilePageController'
        }).
        otherwise({
            redirectTo:'/Profile'
        });
}
configFunction.$inject = ['$routeProvider'];

LevelerAngularMVC.config(configFunction);