﻿var ProfilePageController = function ($scope) {
    $scope.models = {
        'helloAngular': 'I work Andy!'
    };
}

var ProfileController = function ($http, $scope) {

    $scope.templates =
    [{ name: 'billtemplate', url: 'Profile/Bill' },
      { name: 'dealtemplate', url: 'Profile/Deal' }];

    $scope.template = $scope.templates[0];
   
    $scope.GetBills = function () {
        var response = $http.get('/Leveler.Api/Bill');
        response.success(function (data, status, headers, config) {

            $scope.bills = data;
            bill = data;
            $scope.template = $scope.templates[0];
        });

        response.error(function (data, status, headers, config) {
            alert("Error.");
        });
    };

    $scope.GetDeals = function () {
      
        var response = $http.get('/Leveler.Api/Deal');

        //revisit deal api that return json objects
        //var response = $http.get('http://api.8coupons.com/v1/getsubcategory');
        response.success(function (data, status, headers, config) {

            $scope.deals = data;
            $scope.template = $scope.templates[1];
        });

        response.error(function (data, status, headers, config) {
            alert("Error.");
        });
    };


    $scope.saveBill = function (data, id) {

        var billLength = $scope.bills.length;

        //search through array of bills
        // $scope.bills already updated!
        angular.extend(data, { id: id });
        var obj = $scope.bills.filter(function (obj) {
            return obj.Id === id;
        })[0];

        $scope.bill = obj;
        if (id > billLength) {
            $http.post('/Leveler.Api/Bill', $scope.bill).error(function (err) {
                if (err.field && err.msg) {
                    // err like {field: "name", msg: "Server-side error for...!"} 
                    $scope.editableForm.$setError(err.field, err.msg);
                } else {
                    // unknown error
                    $scope.editableForm.$setError('name', 'Unknown error!');
                }
            });
        }
        else {
            $http.put('/Leveler.Api/Bill', $scope.bill).error(function (err) {
                if (err.field && err.msg) {
                    // err like {field: "name", msg: "Server-side error for..."} 
                    $scope.editableForm.$setError(err.field, err.msg);
                } else {
                    // unknown error
                    $scope.editableForm.$setError('name', 'Unknown error!');
                }
            });
        }
    };

    $scope.saveDeal = function (data, id) {
        //search through array of bills
        // $scope.deals already updated!
        var obj = $scope.deals.filter(function (obj) {
            return obj.id === id; 
        })[0];

        $scope.deal= obj;

        return $http.put('/Leveler.Api/Deal', $scope.deal).error(function (err) {
            if (err.field && err.msg) {
                // err like {field: "name", msg: "Server-side error for..."} 
                $scope.editableForm.$setError(err.field, err.msg);
            } else {
                // unknown error
                $scope.editableForm.$setError('Company', 'Unknown error!');
            }
        });
    };

    $scope.addBill=function()
    {
        $scope.inserted = {
            Id: $scope.bills.length + 1,
            HouseId: null,
            Amount: '',
            Reminder: null,
            Status: ''
        };
        $scope.bills.push($scope.inserted);
    }

    $scope.removeBill = function (index) {
        $scope.bills.splice(index, 1);

        var billId = index + 1;
        $http.delete('/Leveler.Api/Bill/' + billId)
    };
};

var TradeController = function ($http, $scope) {
    $scope.GetTrades = function () {
        var response = $http.get('/Leveler.Api/Trade');
        response.success(function (data, status, headers, config) {

            $scope.trades = data;

        });
        response.error(function (data, status, headers, config) {
            alert("Error.");
        });
    };
};

var user;
function SetUser(a,b,c,d,e,f)
{
    user = {
        id:"1",
        name: a,
        last_name: b,
        email: c,
        phonenumber: d,
        username: e,
        password:f
        };
}

function GetUserInfo() { return user; }


var AccountController = function ($scope, $filter, $http) {
    $scope.user = GetUserInfo();

    $scope.saveUser = function() {
        // $scope.user already updated!
        return $http.put('/Leveler.Api/User', $scope.user).error(function (err) {
            if(err.field && err.msg) {
                // err like {field: "name", msg: "Server-side error for..."} 
                $scope.editableForm.$setError(err.field, err.msg);
            } else { 
                // unknown error
                $scope.editableForm.$setError('name', 'Unknown error!');
            }
        });
    };
};

var LoginController = function ($scope, $filter, $http) {
    $scope.user = {};

    $scope.submit = function () {
        alert("Submitted Login");
    };
};

var RegisterController = function ($scope, $filter, $http) {
    $scope.info = {};

    $scope.submit = function () {
        alert("Submitted Register");
    };
};



