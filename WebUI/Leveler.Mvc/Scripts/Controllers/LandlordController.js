﻿var LandlordController = function ($http, $scope) {
    $scope.templates =
    [{ name: 'billtemplate', url: 'Profile/Home' },
      { name: 'Contracttemplate', url: 'Landlord/Contract' }];


    $scope.template = $scope.templates[0];
    $scope.hello = "hello landlord";

    $scope.GetHomes = function () {
       

    };

    $scope.getContracts = function () {
        var response = $http.get('/Leveler.Api/HouseContract');
        response.success(function (data, status, headers, config) {

            $scope.contracts = data;
            $scope.template = $scope.templates[1];
        });

        response.error(function (data, status, headers, config) {
            console.log("Error while getting contracts 'GetContracts'");
            console.log(status);
        });
    };

    
    $scope.saveContract= function(data, id)
    {
        //var contract = document.getElementsByClassName('CodeMirror-scroll')[0];
        //var content = contract.innerHTML;

        var obj = $scope.contracts.filter(function (obj) {
            return obj.id === id;
        })[0];

        $scope.contract = obj;

        $http.post('/Leveler.Api/HouseContract', $scope.contract).error(function (err) {
            if (er=r.field && err.msg) {
                // err like {field: "name", msg: "Server-side error for..."} 
                $scope.editableForm.$setError(err.field, err.msg);
            } else {
                // unknown error
                $scope.editableForm.$setError('name', 'Unknown error!');
            }
        });
    }

    $scope.addContract = function () {
        $scope.inserted = {
            id: $scope.contracts.length++,
            datecreated: null,
            datemodified: null,
            duedate: null,
            houseid: '',
            landlord: null,
            landlordid: '',
            status: '',
            contract:''
        };
        $scope.contracts.push($scope.inserted);
    }

    $scope.deleteContract = function (index) {
        $scope.contracts.splice(++index, 1);
        $http.delete('/Leveler.Api/HouseContract/' + index);
    }
};
